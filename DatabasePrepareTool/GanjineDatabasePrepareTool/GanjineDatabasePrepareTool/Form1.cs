﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace GanjineDatabasePrepareTool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            this.UseWaitCursor = true;

            this.Refresh();

            OpenFileDialog dlg = new OpenFileDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string connectionString = "Data Source=" + dlg.FileName + ";Version=3;";

                var cn = new SQLiteConnection(connectionString);
                cn.Open();

                if (!columnExist("normalizedtext" , "verse", cn))
                {
                    addColumn("normalizedtext", "TEXT",  "verse", cn);
                }

                SQLiteCommand cmd = new SQLiteCommand("UPDATE verse SET normalizedtext = " + StringFunctions.GenerateSqliteNormalizationStatement("text", true), cn);
                cmd.ExecuteNonQuery();

                cn.Close();
            }

            this.UseWaitCursor = false;
            button1.Enabled = true;
        }

        private void addColumn(string column, string colType, string table, SQLiteConnection cn)
        {
            SQLiteCommand cmd = new SQLiteCommand("ALTER TABLE " + table + " ADD COLUMN " + column + " " + colType, cn);
            cmd.ExecuteNonQuery();
        }

        public bool columnExist(string column, string table, SQLiteConnection cn)
        {
            try {
                SQLiteCommand cmd = new SQLiteCommand("select " + column + " from " + table, cn);
                SQLiteDataReader rd = cmd.ExecuteReader();
                rd.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
