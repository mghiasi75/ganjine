﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GanjineDatabasePrepareTool
{
    class StringFunctions
    {

        public static char[][] ArabicChars = { new char[] { 'ك', 'ک' }, new char[] { 'ي', 'ی' }, new char[] { 'آ', 'ا' }, new char[] { 'أ', 'ا' }, new char[] { 'إ', 'ا' }, new char[] { 'ة', 'ه' } };
        public static char[] Spaces = { ' ', '‌' };
        public static char[] RemovableChars = { 'ْ', 'ٌ', 'ٍ', 'ً', 'ُ', 'ِ', 'َ', 'ّ', '.', '،', '٬', '؛', '!', '؟', '?', 'ٔ', ':', '«', '»' };

        public static Dictionary<string, string> correctNumsCache = new Dictionary<string, string>();

        public static string NormalizeString(string s, bool removeSpaces)
        {
            foreach (var item in ArabicChars)
            {
                s = s.Replace(item[0], item[1]);
            }

            foreach (var item in RemovableChars)
            {
                s = s.Replace(item.ToString(), "");
            }

            if (removeSpaces)
            {
                foreach (var item in Spaces)
                {
                    s = s.Replace(item.ToString(), "");
                }
            }
            else
            {
                foreach (var item in Spaces)
                {
                    if (item != ' ')
                        s = s.Replace(item, ' ');
                }
            }

            return s;
        }

        public static string GenerateSqliteNormalizationStatement(string s, bool removeSpaces)
        {
            //return s;
            foreach (var item in ArabicChars)
            {
                s = "replace(" + s + ", '" + item[0] + "', '" + item[1] + "')";
            }
            foreach (var item in RemovableChars)
            {
                s = "replace(" + s + ", '" + item + "', '')";
            }
            if (removeSpaces)
            {
                foreach (var item in Spaces)
                {
                    s = "replace(" + s + ", '" + item + "', '')";
                }
            }
            else
            {
                foreach (var item in Spaces)
                {
                    if (item != ' ')
                        s = "replace(" + s + ", '" + item + "', ' ')";
                }
            }

            return s;
        }

    }
}
