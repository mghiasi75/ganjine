﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GanjineTileUpdater
{
    public sealed class TileTemplates
    {
        public static string PoetTileWithImage
        {
            get
            {
                return @"<tile>
  <visual branding=""none"">

    <binding template=""TileSmall"" hint-textStacking=""center"">
          <image src=""{2}"" hint-crop=""circle""/>
    </binding>

    <binding template=""TileMedium"" hint-textStacking=""center"">
  <group>
        <subgroup hint-weight=""1""/>
        <subgroup hint-weight=""4"">
          <image src=""{2}"" hint-crop=""circle""/>
        </subgroup>
        <subgroup hint-weight=""1""/>
      </group>

    <text hint-style=""base"" hint-align=""center"">{0} {1}</text>


    </binding>

    <binding template=""TileWide"" branding=""name"">
      <group>

        <subgroup hint-textStacking=""center"" hint-weight=""33"">
          <image src=""{2}"" hint-crop=""circle"" />
        </subgroup>
        <subgroup hint-textStacking=""center"">
          <text hint-style=""subtitle"" hint-wrap=""true"">{0}</text>
      <text hint-style=""baseSubtle"">{1}</text>
        </subgroup>
      </group>
    </binding>

    <binding template=""TileLarge"" hint-textStacking=""center"" branding=""name"">
      <group>
        <subgroup hint-weight=""2""/>
        <subgroup hint-weight=""3"">
          <image src=""{2}"" hint-crop=""circle""/>
        </subgroup>
        <subgroup hint-weight=""2""/>
      </group>
      <text hint-style=""subtitle"" hint-align=""center"">{0}</text>
      <text hint-style=""baseSubtle"" hint-align=""center"">{1}</text>
    </binding>


  </visual>
</tile>";
            }
        }


        public static string PoetTileWithImagePhone
        {
            get
            {
                return @"<tile>
  <visual branding=""none"">

    <binding template=""TileSmall"" hint-textStacking=""center"">
          <image src=""{2}"" hint-crop=""circle""/>
    </binding>

    <binding template=""TileMedium"" hint-textStacking=""center"">
  <group>
        <subgroup hint-weight=""1""/>
        <subgroup hint-weight=""4"">
          <image src=""{2}"" hint-crop=""circle""/>
        </subgroup>
        <subgroup hint-weight=""1""/>
      </group>

    <text hint-style=""base"" hint-align=""center"">{0} {1}</text>


    </binding>

    <binding template=""TileWide"" branding=""name"">
      <group>
        <subgroup hint-textStacking=""center"">
          <text hint-style=""subtitle"" hint-wrap=""true"">{0}</text>
      <text hint-style=""baseSubtle"">{1}</text>
        </subgroup>
        <subgroup hint-textStacking=""center"" hint-weight=""33"">
          <image src=""{2}"" hint-crop=""circle"" />
        </subgroup>
      </group>
    </binding>

    <binding template=""TileLarge"" hint-textStacking=""center"" branding=""name"">
      <group>
        <subgroup hint-weight=""2""/>
        <subgroup hint-weight=""3"">
          <image src=""{2}"" hint-crop=""circle""/>
        </subgroup>
        <subgroup hint-weight=""2""/>
      </group>
      <text hint-style=""subtitle"" hint-align=""center"">{0}</text>
      <text hint-style=""baseSubtle"" hint-align=""center"">{1}</text>
    </binding>


  </visual>
</tile>";
            }
        }

        public static string PoetTileNoImage
        {
            get
            {
                return @"<tile>
  <visual branding=""name"">

    <binding template=""TileSmall"" hint-textStacking=""center"" branding=""none"">
      <text hint-style=""caption"" hint-align=""center"">{0}</text>
 <text hint-style=""captionSubtle"" hint-align=""center"">{1}</text>
    </binding>

    <binding template=""TileMedium"" hint-textStacking=""center"">

      <text hint-style=""subtitle"" hint-align=""center"">{0}</text>
 <text hint-style=""baseSubtle"" hint-align=""center"">{1}</text>
    </binding>

    <binding template=""TileWide""  hint-textStacking=""center"">
          <text hint-style=""subtitle"" hint-align=""center"">{0}</text>
      <text hint-style=""baseSubtle"" hint-align=""center"">{1}</text>
    </binding>

    <binding template=""TileLarge"" hint-textStacking=""center"">
      <text hint-style=""header"" hint-align=""center"">{0}</text>
      <text hint-style=""titleSubtle"" hint-align=""center"">{1}</text>
    </binding>


  </visual>
</tile>";
            }
        }


        public static string MainTileImage
        {
            get
            {
                return @"<image src=""{0}""/>";
            }
        }

        public static string MainTilePeople
        {
            get
            {
                return @"
<tile>
  <visual branding=""name"">
    <binding template=""TileMedium"" hint-presentation=""people"">
      {0}
    </binding>
	
    <binding template = ""TileWide"" hint-presentation=""people"">
      {1}
    </binding>
	
    <binding template = ""TileLarge"" hint-presentation=""people"">
      {2}
    </binding>
  </visual>
</tile>
";
            }
        }

        public static int MediumPeopleCount
        {
            get
            {
                return 9;
            }
        }

        public static int WidePeopleCount
        {
            get
            {
                return 15;
            }
        }

        public static int LargePeopleCount
        {
            get
            {
                return 20;
            }
        }
    }
}
