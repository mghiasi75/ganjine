﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace GanjineTileUpdater
{
    public sealed class MainTileHandler
    {
        private static int[] poetsWithPhoto = new int[] { 10, 12, 13, 14, 15, 16, 2, 20, 22, 23, 24, 27, 28, 3, 35, 4, 5, 501, 502, 503, 504, 505, 506, 507, 508, 510, 511, 512, 513, 514, 515, 516, 517, 518, 54, 6, 600, 602, 604, 606, 608, 610, 612, 616, 7, 700, 701, 8, 9 };

        public static void SetMainTileActivity(bool isOn)
        {
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["appMainLiveTile"] = isOn;
        }

        public static bool GetMainTileActivity()
        {
            if (Windows.Storage.ApplicationData.Current.LocalSettings.Values["appMainLiveTile"] == null)
                SetMainTileActivity(true);

            return (bool)Windows.Storage.ApplicationData.Current.LocalSettings.Values["appMainLiveTile"];
        }

        public static void UpdateMainTile()
        {
            if (Windows.Storage.ApplicationData.Current.LocalSettings.Values["appMainLiveTile"] == null)
                SetMainTileActivity(true);

            if (((bool)Windows.Storage.ApplicationData.Current.LocalSettings.Values["appMainLiveTile"]) == false)
            {
                ClearMainTile();
                return;
            }

            string xml = TileTemplates.MainTilePeople;

            xml = xml.Replace("{0}", GetRandomImageXML(TileTemplates.MediumPeopleCount));
            xml = xml.Replace("{1}", GetRandomImageXML(TileTemplates.WidePeopleCount));
            xml = xml.Replace("{2}", GetRandomImageXML(TileTemplates.LargePeopleCount));

            //System.Diagnostics.Debug.WriteLine(xml);

            UpdateMainTileS(xml);
        }

        public static string GetRandomImageXML(int count)
        {
            string output = "";
            int[] poetIDs = GetRandomIDs(count);
            for (int i = 0; i < count; i++)
            {
                output += TileTemplates.MainTileImage.Replace("{0}", "ms-appx:///Assets/" + poetIDs[i].ToString() + ".jpg");
            }
            return output;
        }

        /// <summary>
        /// Picks random selection of available poet photos.
        /// </summary>
        public static int[] GetRandomIDs(int count)
        {
            if (count > poetsWithPhoto.Length)
                return poetsWithPhoto;

            var items = new List<int>(poetsWithPhoto);
            var returnIDs = new List<int>();

            Random r = new Random();
            int chosen = 0;

            while (chosen < count)
            {
                int curIdx = r.Next(0, items.Count);
                returnIDs.Add(items[curIdx]);
                items.RemoveAt(curIdx);

                chosen++;
            }

            return returnIDs.ToArray();
        }

        public static void ClearMainTile()
        {
            TileUpdateManager.CreateTileUpdaterForApplication().Clear();
        }


        public static void UpdateMainTileS(string xml)
        {
            XmlDocument doc;

            doc = new XmlDocument();
            doc.LoadXml(xml);

            UpdateMainTileX(doc);
        }

        public static void UpdateMainTileX(XmlDocument doc)
        {
            TileUpdateManager.CreateTileUpdaterForApplication().Update(new TileNotification(doc));
        }
    }
}
