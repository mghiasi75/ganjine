﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.ApplicationModel.Background;
using Windows.UI.Notifications;

namespace GanjineTileUpdater
{
    public sealed class GanjineBackgroundTask : IBackgroundTask
    {
        //BackgroundTaskDeferral _deferral;
        public void Run(IBackgroundTaskInstance taskInstance)
        {
            //_deferral = taskInstance.GetDeferral(); // Note: define at class scope

            DoIt();

            //_deferral.Complete();
        }

        public static void DoIt()
        {
            MainTileHandler.UpdateMainTile();
            /*
            const ToastTemplateType toastTemplate = ToastTemplateType.ToastText01;
            var toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);

            var toastTextElements = toastXml.GetElementsByTagName("text");
            toastTextElements[0].AppendChild(toastXml.CreateTextNode("Ganjine " + DateTime.Now.ToString()));

            var toast = new ToastNotification(toastXml);
            ToastNotificationManager.CreateToastNotifier().Show(toast);*/
        }
    }

}
