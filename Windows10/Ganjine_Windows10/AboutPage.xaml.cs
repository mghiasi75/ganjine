﻿using Ganjine;
using Ganjine_Windows10.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Email;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Ganjine_Windows10
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AboutPage : Page
    {
        int defaultThemeId = 0;

        public AboutPage()
        {
            this.InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            if (localSettings.Values["appCurTheme"] == null)
                localSettings.Values["appCurTheme"] = 2;

            switch ((int)localSettings.Values["appCurTheme"])
            {
                case 0:
                    themeRadioButtonWhite.IsChecked = true;
                    break;
                case 1:
                    themeRadioButtonBlack.IsChecked = true;
                    break;
                case 2:
                    themeRadioButtonOS.IsChecked = true;
                    break;
                default:
                    themeRadioButtonOS.IsChecked = true;
                    localSettings.Values["appCurTheme"] = 2;
                    break;
            }

            defaultThemeId = ((int)localSettings.Values["appCurTheme"]);

            try
            {
                tileAnimToggleSwitch.IsOn = GanjineTileUpdater.MainTileHandler.GetMainTileActivity();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("**** Couldn't get main tile activity status. " + ex.Message);
            }

            if (localSettings.Values["screenAlwaysOn"] == null)
                localSettings.Values["screenAlwaysOn"] = 0;
            screenOnToggleSwitch.IsOn = (int)localSettings.Values["screenAlwaysOn"] == 0 ? false : true;

            SystemNavigationManager.GetForCurrentView().BackRequested += BackPage;

            var myTracker = GoogleAnalytics.EasyTracker.GetTracker();
            myTracker.SendView("AboutPage");
        }


        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            SystemNavigationManager.GetForCurrentView().BackRequested -= BackPage;

            base.OnNavigatingFrom(e);
        }

        private void themeSelection_Click(object sender, RoutedEventArgs e)
        {
            RadioButton r = (RadioButton)sender;
            int themeId = int.Parse(r.Tag.ToString());
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["appCurTheme"] = themeId;

            if (IsRestartNecessary())
                themeChangeMessage.Opacity = 1;
            else
                themeChangeMessage.Opacity = 0;

            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Settings", "ThemeSelection", themeId.ToString(), 0);
        }

        private async void tileAnimToggleSwitch_Toggled(object sender, RoutedEventArgs e)
        {
            bool isOn = ((ToggleSwitch)sender).IsOn;
            GanjineTileUpdater.MainTileHandler.SetMainTileActivity(((ToggleSwitch)sender).IsOn);
            
            if (isOn)
            {
                await BackgroudTaskHelper.RegisterGanjineBackgroundTask();
                GanjineTileUpdater.MainTileHandler.UpdateMainTile();
            }
            else
            {
                GanjineTileUpdater.MainTileHandler.ClearMainTile();
                BackgroudTaskHelper.UnregisterGanjineBackgroundTask();
            }

            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Settings", "TileAnimation", isOn ? "1" : "0", 0);
        }

        private void BackToggle_Click(object sender, RoutedEventArgs e)
        {
            if (IsRestartNecessary())
                Application.Current.Exit();
            else
                Frame.GoBack();
        }

        private void BackPage(object sender, BackRequestedEventArgs e)
        {
            if (IsRestartNecessary())
            {
                Application.Current.Exit();
                e.Handled = true;
            }
        }

        private bool IsRestartNecessary()
        {
            int themeId = (int)Windows.Storage.ApplicationData.Current.LocalSettings.Values["appCurTheme"];

            ApplicationTheme requestedTheme = (themeId == 2) ? App.systemTheme : (ApplicationTheme)themeId;
            ApplicationTheme currentTheme = (defaultThemeId == 2) ? App.systemTheme : (ApplicationTheme)defaultThemeId;

            if (requestedTheme != currentTheme)
                return true;

            return false;    
        }

        private async void ContactButton_Click(object sender, RoutedEventArgs e)
        {
            EmailMessage mail = new EmailMessage();
            mail.Subject = "Ganjine for Windows, v3.0";
            mail.Body = "";
            mail.To.Add(new Windows.ApplicationModel.Email.EmailRecipient("mghiasi75@gmail.com", "Mahdi Ghiasi"));
            await Windows.ApplicationModel.Email.EmailManager.ShowComposeNewEmailAsync(mail);
        }

        private async void RateButton_Click(object sender, RoutedEventArgs e)
        {
            await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-windows-store:reviewapp?appid=f7ba92a3-ca3c-4252-84ab-82eb3e4b0c62"));
        }

        private void screenOnToggleSwitch_Toggled(object sender, RoutedEventArgs e)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            int val = (screenOnToggleSwitch.IsOn) ? 1 : 0;
            localSettings.Values["screenAlwaysOn"] = val;

            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Settings", "ScreenOn", val.ToString(), 0);
        }
    }
}
