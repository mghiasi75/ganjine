﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;

namespace Ganjine_Windows10.Common
{
    class BackgroudTaskHelper
    {
        public static bool IsBackgroudTaskRegistered(string taskName)
        {
            foreach (var task in BackgroundTaskRegistration.AllTasks)
                if (task.Value.Name == taskName)
                    return true;

            return false;
        }

        public static async Task<BackgroundTaskRegistration> RegisterBackgroundTask(string taskName, string taskEntryPoint, uint interval)
        {
            if (IsBackgroudTaskRegistered(taskName))
                return null;

            var builder = new BackgroundTaskBuilder();

            builder.Name = taskName;
            builder.TaskEntryPoint = taskEntryPoint;
            builder.SetTrigger(new TimeTrigger(interval, false));

            await BackgroundExecutionManager.RequestAccessAsync();

            BackgroundTaskRegistration task = builder.Register();

            return task;
        }

        public static void UnregisterBackgroundTask(string taskName)
        {
            foreach (var task in BackgroundTaskRegistration.AllTasks)
                if (task.Value.Name == taskName)
                    task.Value.Unregister(true);
        }

        public static async Task RegisterGanjineBackgroundTask()
        {
            try
            {
                if (GanjineTileUpdater.MainTileHandler.GetMainTileActivity())
                    await BackgroudTaskHelper.RegisterBackgroundTask("ganjineTileBackgroundTask", "GanjineTileUpdater.GanjineBackgroundTask", 30);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("**** Failed to register background task: " + ex.Message);
            }
        }

        public static void UnregisterGanjineBackgroundTask()
        {
            UnregisterBackgroundTask("ganjineTileBackgroundTask");
        }
    }
}
