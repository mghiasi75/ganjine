﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace Ganjine_Windows10.Common
{
    class SecondaryTileUpdater
    {

        public static async Task UpdateTiles()
        {
            List<string> deletedTiles = new List<string>();
            var tiles = Windows.Storage.ApplicationData.Current.LocalSettings.Values.Where(x => x.Key.Contains("TileXml_"));

            foreach (var item in tiles)
            {
                string tileId = item.Key.Replace("TileXml_", "");
                if (await TilesHelper.TileExists(tileId))
                    UpdateSecondaryTile(item.Value.ToString(), tileId);
                else
                    deletedTiles.Add(tileId);
            }

            foreach (var item in deletedTiles)
            {
                if (Windows.Storage.ApplicationData.Current.LocalSettings.Values.ContainsKey("TileXml_" + item))
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values.Remove("TileXml_" + item);
                if (Windows.Storage.ApplicationData.Current.LocalSettings.Values.ContainsKey("TileData_" + item))
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values.Remove("TileData_" + item);
            }
        }


        public static void UpdateSecondaryTile(string xml, string tileId)
        {
            XmlDocument doc;

            doc = new XmlDocument();
            doc.LoadXml(xml);

            UpdateSecondaryTile(doc, tileId);
        }

        public static void UpdateSecondaryTile(XmlDocument doc, string tileId)
        {
            TileUpdateManager.CreateTileUpdaterForSecondaryTile(tileId).Clear();
            TileUpdateManager.CreateTileUpdaterForSecondaryTile(tileId).Update(new TileNotification(doc));
        }
    }
}
