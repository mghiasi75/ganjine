﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.UI.StartScreen;

namespace Ganjine_Windows10.Common
{

    public sealed class TilesHelper
    {
        /**
        public static async Task<SecondaryTile> PinNewSecondaryTile()
        {
            SecondaryTile tile = GenerateSecondaryTile("Secondary tile");

            await tile.RequestCreateAsync();

            return tile;
        }
        /**/

        public static SecondaryTile GenerateSecondaryTile(string tileId, string displayName, string args, string logo)
        {
            SecondaryTile tile = new SecondaryTile(tileId, displayName, args, new Uri(logo), TileSize.Default);

            tile.VisualElements.ShowNameOnSquare150x150Logo = true;
            tile.VisualElements.ShowNameOnWide310x150Logo = true;
            tile.VisualElements.ShowNameOnSquare310x310Logo = true;
            tile.VisualElements.Wide310x150Logo = tile.VisualElements.Square150x150Logo;
            tile.VisualElements.Square310x310Logo = tile.VisualElements.Square150x150Logo;
            

            return tile;
        }

        /**
        public static SecondaryTile GenerateSecondaryTile(string displayName)
        {
            return GenerateSecondaryTile(DateTime.Now.Ticks.ToString(), displayName);
        }
        /**/

        public static async Task<bool> TileExists(string tileId)
        {
            return ((await SecondaryTile.FindAllAsync()).Where(i => i.TileId.Equals(tileId)).Count() > 0);
        }

        public static async Task<SecondaryTile> FindExisting(string tileId)
        {
            return (await SecondaryTile.FindAllAsync()).FirstOrDefault(i => i.TileId.Equals(tileId));
        }

        public static async Task<SecondaryTile> PinNewSecondaryTile(string tileId, string displayName, string xml, string args, string logo)
        {
            SecondaryTile tile = GenerateSecondaryTile(tileId, displayName, args, logo);
            await tile.RequestCreateAsync();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            TileUpdateManager.CreateTileUpdaterForSecondaryTile(tile.TileId).Update(new TileNotification(doc));

            return tile;
        }

        public static async Task UpdateTiles(string tileId, string xml, string displayName, string args, string logo)
        {
            XmlDocument doc;

            try
            {
                doc = new XmlDocument();
                doc.LoadXml(xml);
            }

            catch (Exception ex)
            {
                await new MessageDialog(ex.ToString(), "ERROR: Invalid XML").ShowAsync();
                return;
            }

            await UpdateTiles(tileId, doc, displayName, args, logo);
        }

        public static async Task UpdateTiles(string tileId, XmlDocument doc, string displayName, string args, string logo)
        {
            try
            {
                TileUpdateManager.CreateTileUpdaterForApplication().Update(new TileNotification(doc));

                await UpdateTile(tileId, doc, displayName, args, logo);
            }

            catch (Exception ex)
            {
                await new MessageDialog(ex.ToString(), "ERROR: Failed updating tiles").ShowAsync();
            }
        }

        private static async Task UpdateTile(string tileId, XmlDocument doc, string displayName, string args, string logo)
        {
            if (!SecondaryTile.Exists(tileId))
            {
                SecondaryTile tile = GenerateSecondaryTile(tileId, displayName, args, logo);
                tile.VisualElements.ShowNameOnSquare310x310Logo = true;
                await tile.RequestCreateAsync();
            }
            
            TileUpdateManager.CreateTileUpdaterForSecondaryTile(tileId).Update(new TileNotification(doc));
        }

        public static async Task RemoveTile(string tileId)
        {
            SecondaryTile tile = await FindExisting(tileId);
            await tile.RequestDeleteAsync();
        }

    }
}
