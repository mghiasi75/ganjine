﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Ganjine_Windows10.Common
{
    class DefaultUserAgent
    {
        private static string s_userAgent = null;

        // Get the default UserAgent which webviews use on this platform.
        public static async Task<string> GetUserAgent()
        {
            if (s_userAgent == null)
            {
                const string Html = @"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN"">
                                <html>
                                <head>
                                    <script language=""javascript"" type=""text/javascript"">
                                        function notifyUA() { window.external.notify(navigator.userAgent); } 
                                    </script>
                                </head>
                                <body onload=""notifyUA();""></body>
                                </html>";

                SemaphoreSlim signal_done = new SemaphoreSlim(0, 1);

                var wv = new WebView();
                wv.ScriptNotify += (sender, args) =>
                {
                    s_userAgent = args.Value;
                    // set signal, to show we've done
                    signal_done.Release();
                };
                wv.NavigateToString(Html);

                // wait for signal 
                await signal_done.WaitAsync();

            }

            return s_userAgent;
        }
    }
}
