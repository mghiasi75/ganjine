﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ganjine_Windows10.Common
{
    class StringFunctions
    {
        public static char[] EnNumbers = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        public static char[] FaNumbers = { '۰', '۱', '۲', '۳', '۴', '٤', '۵', '٥', '۶', '٦', '۷', '۸', '۹'};

        public static char[][] ArabicChars = { new char[] { 'ك', 'ک' }, new char[] { 'ي', 'ی' }, new char[] { 'آ', 'ا' }, new char[] { 'أ', 'ا' }, new char[] { 'إ', 'ا' }, new char[] { 'ة', 'ه' } };
        public static char[] Spaces = { ' ', '‌' };
        public static char[] RemovableChars = { 'ْ', 'ٌ', 'ٍ', 'ً', 'ُ', 'ِ', 'َ', 'ّ', '.', '،', '٬', '؛', '!', '؟', '?', 'ٔ', ':', '«', '»' };

        public static Dictionary<string, string> correctNumsCache = new Dictionary<string, string>();

        public static string NormalizeString(string s, bool removeSpaces)
        {
            foreach (var item in ArabicChars)
            {
                s = s.Replace(item[0], item[1]);
            }

            foreach (var item in RemovableChars)
            {
                s = s.Replace(item.ToString(), "");
            }

            if (removeSpaces)
            {
                foreach (var item in Spaces)
                {
                    s = s.Replace(item.ToString(), "");
                }
            }
            else
            {
                foreach (var item in Spaces)
                {
                    if (item != ' ')
                        s = s.Replace(item, ' ');
                }
            }

            return s;
        }

        public static string GenerateSqliteNormalizationStatement(string s, bool removeSpaces)
        {
            //return s;
            foreach (var item in ArabicChars)
            {
                s = "replace(" + s + ", '" + item[0] + "', '" + item[1] + "')";
            }
            foreach (var item in RemovableChars)
            {
                s = "replace(" + s + ", '" + item + "', '')";
            }
            if (removeSpaces)
            {
                foreach (var item in Spaces)
                {
                    s = "replace(" + s + ", '" + item + "', '')";
                }
            }
            else
            {
                foreach (var item in Spaces)
                {
                    if (item != ' ')
                        s = "replace(" + s + ", '" + item + "', ' ')";
                }
            }

            return s;
        }

        public static string correctNums(string s, char[] Nums, int numLen = 10)
        {
            try {
                int numBegin = 0;
                bool inNums = false;
                for (int i = 0; i < s.Length + 1; i++)
                {
                    if ((i != s.Length) && (Nums.Contains(s[i])))
                    {
                        if (!inNums)
                            numBegin = i;

                        inNums = true;
                    }
                    else
                    {
                        if (inNums)
                        {
                            int numEnd = i;

                            int l = numLen - (numEnd - numBegin);
                            string num = "";
                            for (int j = 0; j < l; j++)
                                num += Nums[0];

                            s = s.Insert(numBegin, num);

                            i += l;

                            inNums = false;
                        }
                    }
                }

                return s;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("correctNums exception: " + ex.Message);
                return s;
            }
        }

        public static string correctNums(string s, int numLen = 10)
        {
            if (correctNumsCache.ContainsKey(s))
                return correctNumsCache[s];

            string s2;
            s2 = correctNums(s, EnNumbers, numLen);
            s2 = correctNums(s2, FaNumbers, numLen);
            correctNumsCache.Add(s, s2);

            return s2;
        }
    }
}
