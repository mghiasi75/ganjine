﻿using Ganjine.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Ganjine_Windows10
{
    class SectionPageTemplateSelector : DataTemplateSelector
    {
        public DataTemplate SectionTemplate { get; set; }
        public DataTemplate PoemTemplate { get; set; }
        public DataTemplate TextTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            var dataItem = item as PoemItem;

            if (dataItem.HasAdditionalData == true)
            {
                if ((dataItem.s2 != null) && (dataItem.s2.Length > 0))
                    return PoemTemplate;
                else
                    return TextTemplate;
            }

            return SectionTemplate;

        }
    }
}
