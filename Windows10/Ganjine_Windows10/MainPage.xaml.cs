﻿using Ganjine.Common;
using Ganjine.DataModel;
using Ganjine_Windows10;
using Ganjine_Windows10.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Ganjine
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private readonly string defaultPoetOrder = @"{""2"":0,""3"":1,""4"":2,""5"":3,""6"":4,""7"":5,""8"":6,""9"":7,""10"":8,""11"":9,""12"":10,""13"":11,""14"":12,""15"":13,""27"":14,""28"":15,""16"":16,""35"":17,""700"":18,""501"":19,""502"":20,""503"":21,""504"":22,""506"":23,""701"":24,""505"":25,""507"":26,""513"":27,""516"":28,""514"":29,""615"":30,""57"":31,""26"":32,""601"":33,""52"":34,""42"":35,""600"":36,""34"":37,""18"":38,""19"":39,""56"":40,""614"":41,""613"":42,""43"":43,""618"":44,""24"":45,""517"":46,""610"":47,""48"":48,""20"":49,""47"":50,""41"":51,""40"":52,""518"":53,""604"":54,""31"":55,""55"":56,""612"":57,""51"":58,""30"":59,""23"":60,""608"":61,""616"":62,""22"":63,""512"":64,""37"":65,""33"":66,""21"":67,""46"":68,""606"":69,""38"":70,""32"":71,""39"":72,""44"":73,""609"":74,""611"":75,""45"":76,""29"":77,""508"":78,""510"":79,""17"":80,""617"":81,""602"":82,""511"":83,""54"":84,""515"":85,""603"":86,""49"":87,""605"":88,""25"":89,""53"":90,""50"":91}";
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        ObservableCollection<PoetItem> poetsCollection;
        bool isNavigatedBack = false;

        public MainPage()
        {
            this.InitializeComponent();
            Windows.UI.ViewManagement.ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size { Width = 320, Height = 400 });
        }

        /// <summary>
        /// Gets the DefaultViewModel. This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        private void PhoneSearchHardwareBackButtonPressed(object sender, BackRequestedEventArgs e)
        {
            e.Handled = true;
            BackToggle_Click(sender, new RoutedEventArgs());
        }

        private void SearchToggle_Click(object sender, RoutedEventArgs e)
        {
            headerText.Visibility = Visibility.Collapsed;
            SearchToggle.Visibility = Visibility.Collapsed;
            BackToggle.Visibility = Visibility.Visible;
            SearchBar.Visibility = Visibility.Visible;

            SearchTextBox.Focus(FocusState.Pointer);
            PhoneMainContent.Visibility = Visibility.Collapsed;
            PhoneCommandBar.Visibility = Visibility.Collapsed;

            SystemNavigationManager.GetForCurrentView().BackRequested += PhoneSearchHardwareBackButtonPressed;
        }

        private void BackToggle_Click(object sender, RoutedEventArgs e)
        {
            headerText.Visibility = Visibility.Visible;
            SearchToggle.Visibility = Visibility.Visible;
            BackToggle.Visibility = Visibility.Collapsed;
            SearchBar.Visibility = Visibility.Collapsed;

            SearchTextBox.Text = "";
            PhoneMainContent.Visibility = Visibility.Visible;
            PhoneCommandBar.Visibility = Visibility.Visible;

            SystemNavigationManager.GetForCurrentView().BackRequested -= PhoneSearchHardwareBackButtonPressed;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            SearchButton.Focus(FocusState.Programmatic); //Drive focus away from SearchBox, to avoid keyboard appearing in the beginning of app in tablets.

            SetSearchButtonOpacity();

            await CopyDatabaseAsync();
            await App.dataSource.InitializeFavorites();

            UpdateFavSelectedOrderText();

            PhoneMainContentPivot.IsEnabled = true;

            LoadingRing.Visibility = Visibility.Collapsed;

            //Create connection with database
            App.dataSource.CheckDBConnection();

            if ((!this.defaultViewModel.ContainsKey("Poets")) || (defaultViewModel["Poets"] == null))
            {
                //Read list of poets.
                Dictionary<int, int> orders;
                poetsCollection = App.dataSource.GetListOfPoets();


                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;


                try
                {
                    if (localSettings.Values["poetOrders"] == null) //Default order
                    {
                        localSettings.Values["poetOrders"] = defaultPoetOrder;
                    }
                    orders = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<int, int>>(localSettings.Values["poetOrders"].ToString());

                    int maxVal = orders.Values.Max();

                    foreach (var item in poetsCollection)
                    {
                        if (!orders.ContainsKey(item.PoetID))
                        {
                            maxVal++;
                            orders.Add(item.PoetID, maxVal);
                        }

                        item.OrderID = orders[item.PoetID];
                    }

                    poetsCollection = new ObservableCollection<PoetItem>(poetsCollection.OrderBy(a => a.OrderID));

                    poetsCollection.CollectionChanged += PoetsCollection_CollectionChanged;

                    this.defaultViewModel["Poets"] = poetsCollection;
                }
                catch (Exception ex)
                {
                    localSettings.Values["poetOrders"] = null; //Something's wrong. Let's reset data. Things will hopefully get fixed in the next app start.
                    throw ex;
                }


                /* !!!    try { searchSource.SelectedIndex = 0; }
                    catch { } */

                await App.dataSource.InitializeSettings();
            }

            ShowFavorites();


            if (isNavigatedBack)
            {
                CorrectScrollPosition();
                await Task.Delay(50);
                CorrectScrollPosition();

                isNavigatedBack = false;
            }

            ShowPoetsStoryboard.Begin();

            await BackgroudTaskHelper.RegisterGanjineBackgroundTask();

            if (App.askForReview)
            {
                MessageDialog md = new MessageDialog("امتیازدهی و نظرات شما، به ما در بهبود این برنامه کمک خواهد کرد.", "آیا مایلید به گنجینه امتیاز دهید؟");
                md.Commands.Add(new UICommand("خیر") { Id = 0 });
                md.Commands.Add(new UICommand("بله") { Id = 1 });
                md.DefaultCommandIndex = 1;
                md.CancelCommandIndex = 0;

                var result = await md.ShowAsync();

                if (((int)result.Id) == 1)
                {
                    await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-windows-store:reviewapp?appid=f7ba92a3-ca3c-4252-84ab-82eb3e4b0c62"));
                }

                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("AskForReview", "UserResponse", result.Id.ToString(), 0 );

                Windows.Storage.ApplicationData.Current.LocalSettings.Values["AskedForReview"] = 1;
            }

            GanjineTileUpdater.MainTileHandler.UpdateMainTile();
            await SecondaryTileUpdater.UpdateTiles();

            var myTracker = GoogleAnalytics.EasyTracker.GetTracker();
            myTracker.UserAgentOverride = await DefaultUserAgent.GetUserAgent();
            myTracker.SendView("MainPage");

            /*

            await App.dataSource.InitializeFavorites();
            List<PoemItem> ppa = new List<PoemItem>();
            ppa.AddRange(App.favorites);
            this.defaultViewModel["Favs"] = ppa;

            try
            {
                if (ppa.Count == 0)
                {
                    NoFavorites.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
                else
                {
                    NoFavorites.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                }
            }
            catch { }

            initProgress.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            */
        }

        private void UpdateFavSelectedOrderText()
        {
            if (Windows.Storage.ApplicationData.Current.LocalSettings.Values["favOrderBy"] == null)
                Windows.Storage.ApplicationData.Current.LocalSettings.Values["favOrderBy"] = 1;

            foreach (var item in FavOrderFlyoutBase.Items)
            {
                if (item.Tag.ToString() == Windows.Storage.ApplicationData.Current.LocalSettings.Values["favOrderBy"].ToString())
                {
                    FavPoemsOrder.Content = ((MenuFlyoutItem)item).Text;
                    FavPoemsOrderMobile.Content = ((MenuFlyoutItem)item).Text;
                    return;
                }
            }


            System.Diagnostics.Debug.WriteLine("Invalid favOrderBy value (" + Windows.Storage.ApplicationData.Current.LocalSettings.Values["favOrderBy"] + "). Will reset the value.");
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["favOrderBy"] = 1;
            foreach (var item in FavOrderFlyoutBase.Items)
                if (item.Tag.ToString() == Windows.Storage.ApplicationData.Current.LocalSettings.Values["favOrderBy"].ToString())
                {
                    FavPoemsOrder.Content = ((MenuFlyoutItem)item).Text;
                    FavPoemsOrderMobile.Content = ((MenuFlyoutItem)item).Text;
                }
        }

        private void CorrectScrollPosition()
        {
            var gridView = VisualChildFinder.FindVisualChild<ScrollViewer>(PoetsGridView);
            var listView = VisualChildFinder.FindVisualChild<ScrollViewer>(PoetsListView);
            var favListView = VisualChildFinder.FindVisualChild<ScrollViewer>(favItemListView);
            var favListViewMobile = VisualChildFinder.FindVisualChild<ScrollViewer>(favItemListViewMobile);

            if (gridView != null)
                gridView.ChangeView(gridView.HorizontalOffset, App.scrollPosition["MainPageGrid"], gridView.ZoomFactor, true);
            if (listView != null)
                listView.ChangeView(listView.HorizontalOffset, App.scrollPosition["MainPageList"], listView.ZoomFactor, true);
            if (favListView != null)
                favListView.ChangeView(favListView.HorizontalOffset, App.scrollPosition["MainPageFavList"], favListView.ZoomFactor, true);
            if (favListViewMobile != null)
                favListViewMobile.ChangeView(favListViewMobile.HorizontalOffset, App.scrollPosition["MainPageFavListMob"], favListViewMobile.ZoomFactor, true);

            //PivotPage is checked in OnNavigatedTo event, for better performace.
        }

        private void PoetsCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            var orders = new Dictionary<int, int>();
            for (int i = 0; i < poetsCollection.Count; i++)
            {
                poetsCollection[i].OrderID = i;
                orders.Add(poetsCollection[i].PoetID, i);
            }
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            localSettings.Values["poetOrders"] = Newtonsoft.Json.JsonConvert.SerializeObject(orders);

            //TODO: Should we refresh favorites list now? or let changes be affected next time this page opens?
            /**
            Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                await Task.Delay(500);
                ShowFavorites(); 
            });
            /**/
        }

        private async Task CopyDatabaseAsync()
        {
            if (App.conn != null)
            {
                return;
            }

            //SQLite now works with InstalledLocation too. So no need to copy it.
            try
            {
                var file = await ApplicationData.Current.LocalFolder.TryGetItemAsync("ganjoor2.s3db") as IStorageFile;
                if (file != null)
                    await file.DeleteAsync(StorageDeleteOption.PermanentDelete);
            }
            catch { }
            return;

            bool versionMismatch = false;
            bool isDatabaseExisting = false;
            StorageFile storageFile = null;
            StorageFile databaseFile;

            try
            {
                databaseFile = await Package.Current.InstalledLocation.GetFileAsync("ganjoor2.s3db");
            }
            catch (Exception ex)
            {
                throw new Exception("Couldn't get db file.", ex);
            }

            try
            {
                var versionFile = await ApplicationData.Current.LocalFolder.GetFileAsync("version.txt");
                var currentVersionFile = await Package.Current.InstalledLocation.GetFileAsync("version.txt");
                int version = int.Parse(await Windows.Storage.FileIO.ReadTextAsync(versionFile));
                int currentVersion = int.Parse(await Windows.Storage.FileIO.ReadTextAsync(currentVersionFile));
                if (version != currentVersion)
                {
                    versionMismatch = true;
                }
            }
            catch (Exception ex)
            {
                /*LoadingGrid.Visibility = Windows.UI.Xaml.Visibility.Visible;
                commandBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Hub.IsEnabled = false;*/

                versionMismatch = true;
            }

            try
            {
                storageFile = await ApplicationData.Current.LocalFolder.GetFileAsync("ganjoor2.s3db");
                if (versionMismatch)
                {
                    try
                    {
                        App.conn.Dispose();
                        await Task.Delay(TimeSpan.FromSeconds(0.5));
                    }
                    catch { }
                    await storageFile.DeleteAsync();
                }
                else
                {
                    isDatabaseExisting = true;
                    if ((await storageFile.GetBasicPropertiesAsync()).Size != (await databaseFile.GetBasicPropertiesAsync()).Size) //Size mismatch
                    {
                        versionMismatch = true;
                    }
                }
            }
            catch (Exception ex)
            {
                /*LoadingGrid.Visibility = Windows.UI.Xaml.Visibility.Visible;
                commandBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Hub.IsEnabled = false;*/

                isDatabaseExisting = false;
            }

            if (versionMismatch)
            {
                var currentVersionFile = await Package.Current.InstalledLocation.GetFileAsync("version.txt");

                //Delete old version file, if exists.
                try
                {
                    var vFile = await ApplicationData.Current.LocalFolder.GetFileAsync("version.txt");
                    await vFile.DeleteAsync();
                }
                catch
                {

                }

                await currentVersionFile.CopyAsync(ApplicationData.Current.LocalFolder);
                /*if (isDatabaseExisting == true)
                {
                    LoadingGrid.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    commandBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    Hub.IsEnabled = false;
                    bool CriticalError = false;
                    
                    try
                    {
                        storageFile = null;
                        storageFile = await ApplicationData.Current.LocalFolder.GetFileAsync("ganjoor2.s3db");
                        await storageFile.DeleteAsync();
                    }
                    catch (Exception ex)
                    {
                        CriticalError = true;
                    }
                    if (CriticalError)
                    {
                        Hub.Visibility = Visibility.Collapsed;
                        LoadingGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        MessageDialog md = new MessageDialog("مشکلی در راه‌اندازی اولیه‌ی برنامه پیش آمده است. با عرض پوزش، لطفاً برنامه را از گوشی خود پاک کرده و از نو نصب کنید.");
                        await md.ShowAsync();
                        App.Current.Exit();
                        return;
                    }
                    isDatabaseExisting = false;
                }*/
            }

            if (!isDatabaseExisting)
            {


                try
                {
                    //StorageFile newfile = await ApplicationData.Current.LocalFolder.CreateFileAsync("ganjoor2.s3db");
                    //await CopyBigFile(databaseFile, newfile, CancellationToken.None);
                    await databaseFile.CopyAsync(ApplicationData.Current.LocalFolder, "ganjoor2.s3db", NameCollisionOption.ReplaceExisting);
                }
                catch (Exception ex)
                {
                    throw new Exception("Couldn't copy database file to local cache folder.", ex);
                }
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            var gridView = VisualChildFinder.FindVisualChild<ScrollViewer>(PoetsGridView);
            var listView = VisualChildFinder.FindVisualChild<ScrollViewer>(PoetsListView);
            var favListView = VisualChildFinder.FindVisualChild<ScrollViewer>(favItemListView);
            var favListViewMobile = VisualChildFinder.FindVisualChild<ScrollViewer>(favItemListViewMobile);

            if (gridView != null)
                App.scrollPosition["MainPageGrid"] = gridView.VerticalOffset;
            else
                App.scrollPosition["MainPageGrid"] = 0;

            if (listView != null)
                App.scrollPosition["MainPageList"] = listView.VerticalOffset;
            else
                App.scrollPosition["MainPageList"] = 0;

            if (favListView != null)
                App.scrollPosition["MainPageFavList"] = favListView.VerticalOffset;
            else
                App.scrollPosition["MainPageFavList"] = 0;

            if (favListViewMobile != null)
                App.scrollPosition["MainPageFavListMob"] = favListViewMobile.VerticalOffset;
            else
                App.scrollPosition["MainPageFavListMob"] = 0;

            App.scrollPosition["PivotPage"] = PhoneMainContentPivot.SelectedIndex;

            base.OnNavigatedFrom(e);
        }
        
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back)
            {
                isNavigatedBack = true;

                if (App.scrollPosition.ContainsKey("PivotPage"))
                    PhoneMainContentPivot.SelectedIndex = (int)App.scrollPosition["PivotPage"];
            }
            base.OnNavigatedTo(e);
        }

        private void itemGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (AppBarButtonSort.IsChecked == true)
                return;

            var poet = (PoetItem)(e.ClickedItem);
            var db = new DataBridge1
            {
                id = poet.CatID,
                title = poet.PoetName,
                poetid = poet.PoetID,
                isRootItem = true
            };
            Frame.Navigate(typeof(SectionPage), Newtonsoft.Json.JsonConvert.SerializeObject(db));
        }

        private void SortAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            bool isChecked = (((AppBarToggleButton)sender).IsChecked == true);
            PoetsListView.CanReorderItems = isChecked;
            PoetsListView.CanDragItems = isChecked;
            PoetsListView.AllowDrop = isChecked;

            PhoneMainContentPivot.IsLocked = isChecked;

            if (isChecked)
            {
                PoetsListViewAnim.Begin();
                AppBarButtonRevertSortPhone.Visibility = Visibility.Visible;
            }
            else
            {
                PoetsListViewAnimReverse.Begin();
                AppBarButtonRevertSortPhone.Visibility = Visibility.Collapsed;
            }
        }


        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            if (SearchTextBox.Text.Length >= App._minSearchTextLength)
                Frame.Navigate(typeof(Search), Newtonsoft.Json.JsonConvert.SerializeObject(new DataBridgeSearch(SearchTextBox.Text, -1)));
        }

        private void SearchTextBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                SearchButton_Click(sender, new RoutedEventArgs());
            }
        }


        private void KeyPress(Windows.UI.Core.CoreWindow sender, Windows.UI.Core.CharacterReceivedEventArgs args)
        {
            if (FocusManager.GetFocusedElement() is TextBox)
                return;

            if ((SearchTextBox.FocusState == Windows.UI.Xaml.FocusState.Unfocused) &&
                (SearchBar.Visibility == Visibility.Visible))
            {
                Char c = Convert.ToChar(args.KeyCode);
                if (Char.IsLetterOrDigit(c))
                {
                    SearchTextBox.Text = c.ToString();
                    SearchTextBox.SelectionStart = SearchTextBox.Text.Length;
                    SearchTextBox.SelectionLength = 0;
                    SearchTextBox.Focus(FocusState.Programmatic);
                }
            }
        }


        private void KeyPressEventRegister()
        {
            if (SearchBar.Visibility == Visibility.Visible)
                Window.Current.CoreWindow.CharacterReceived += KeyPress;
            else
                Window.Current.CoreWindow.CharacterReceived -= KeyPress;
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            Window.Current.CoreWindow.CharacterReceived -= KeyPress;

            try
            {
                poetsCollection.CollectionChanged -= PoetsCollection_CollectionChanged;
            }
            catch { } //Sometimes a wierd error happens when launching from secondary tile :/

            base.OnNavigatingFrom(e);
        }

        private void pageRoot_GotFocus(object sender, RoutedEventArgs e)
        {
            KeyPressEventRegister();
        }

        private void pageRoot_LostFocus(object sender, RoutedEventArgs e)
        {
            //Window.Current.CoreWindow.CharacterReceived -= KeyPress;
            //System.Diagnostics.Debug.WriteLine("LostFocus");
        }

        private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            KeyPressEventRegister();
        }

        private void SetSearchButtonOpacity()
        {
            SearchButton.Opacity = (SearchTextBox.Text.Length >= App._minSearchTextLength) ? 1 : 0.3;
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetSearchButtonOpacity();
        }

        private void SearchTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if ((SearchBar.HorizontalAlignment == HorizontalAlignment.Stretch) &&
                (SearchButton.FocusState == FocusState.Unfocused))
                BackToggle_Click(sender, e);
        }

        private void FavItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var pi = (PoemItem)e.ClickedItem;
            var db = new DataBridge2
            {
                parentid = 0,
                id = pi.ID,
                HideNextPrevKeys = true,
                title = pi.Text
            };
            Frame.Navigate(typeof(ItemPage), Newtonsoft.Json.JsonConvert.SerializeObject(db));
        }

        /**/
        private void ShowFavorites()
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            if (localSettings.Values["favOrderBy"] == null)
            {
                localSettings.Values["favOrderBy"] = 2;
            }

            var ppa = new List<PoemItem>();
            ppa.AddRange(App.favorites);


            bool orderByName = false;
            if ((int)localSettings.Values["favOrderBy"] == 1)
            {
                /*List<KeyValuePair<string, PoemItem>> items = (from p in ppa
                                                              select new KeyValuePair<string, PoemItem>(p.Text, p)).ToList();
                items.OrderBy(y => y.)                                                                 
                */
                orderByName = true;
            }
            else if ((int)localSettings.Values["favOrderBy"] == 2)
            {
                for (int i = 0; i < ppa.Count; i++)
                    ppa[i].OrderID = i;
            }
            else if ((int)localSettings.Values["favOrderBy"] == 3)
            {
                for (int i = 0; i < ppa.Count; i++)
                    ppa[i].OrderID = -i;
            }

            var groupedFavorites = (ppa.GroupBy(x => x.PoetName).Select(x => new PoemItemGroup { PoetName = x.Key, Items = (orderByName ? (x.OrderBy(y => StringFunctions.correctNums(y.Text)).ToList()) : (x.OrderBy(y => y.OrderID).ToList())) }))
                                    /*.OrderBy(x => poetsCollection.Where(y => y.PoetName == x.PoetName).FirstOrDefault().OrderID)*/.ToList();

            if (ppa.Count == 0)
                FavoritesIsEmptyMessage.Visibility = Visibility.Visible;
            else
                FavoritesIsEmptyMessage.Visibility = Visibility.Collapsed;
            FavoritesIsEmptyMessageMobile.Visibility = FavoritesIsEmptyMessage.Visibility;

            cvsFavorites.Source = groupedFavorites;
        }

        private void FavOrderFlyout_Click(object sender, RoutedEventArgs e)
        {
            var item = ((MenuFlyoutItem)sender);
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["favOrderBy"] = int.Parse(item.Tag.ToString());
            FavPoemsOrder.Content = item.Text;
            FavPoemsOrderMobile.Content = item.Text;
            ShowFavorites();
        }

        private async void RevertSortAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            MessageDialog dlg = new MessageDialog("آیا مطمئنید که می‌خواهید چینش شاعران به حالت پیش‌فرض بازگردد؟", "بازگردانی چینش شاعران");
            dlg.Commands.Add(new UICommand("خیر") { Id = 0 });
            dlg.Commands.Add(new UICommand("بله") { Id = 1 });
            dlg.DefaultCommandIndex = 1;
            dlg.CancelCommandIndex = 0;

            var result = await dlg.ShowAsync();

            if (((int)result.Id) == 1)
            {
                this.defaultViewModel["Poets"] = null;

                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                localSettings.Values["poetOrders"] = defaultPoetOrder;

                Page_Loaded(this, new RoutedEventArgs());
            }
        }

        private void SideAppBarSettings_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(AboutPage));
        }

        private void PhoneMainContentPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Pivot p = (Pivot)sender;
            AppBarButtonSort.Visibility = (p.SelectedIndex == 0) ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
