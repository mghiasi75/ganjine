﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Settings Flyout item template is documented at http://go.microsoft.com/fwlink/?LinkId=273769

namespace Ganjine
{
    public sealed partial class MyAboutFlyout : SettingsFlyout
    {
        public MyAboutFlyout()
        {
            this.InitializeComponent();

            GoogleAnalytics.EasyTracker.GetTracker().SendView("AboutPage");
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var mailto = new Uri("mailto:?to=mghiasi75@gmail.com&subject=Ganjineh for Windows v1.6");
            await Windows.System.Launcher.LaunchUriAsync(mailto);
        }

        private void SettingsFlyout_BackClick(object sender, BackClickEventArgs e)
        {
            this.Hide();
        }
    }
}
