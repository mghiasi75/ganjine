﻿using Ganjine.DataModel;
using Ganjine.Common;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.Web;
using Windows.Storage.Streams;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;

// The Universal Hub Application project template is documented at http://go.microsoft.com/fwlink/?LinkID=391955

namespace Ganjine
{
    /// <summary>
    /// A page that displays details for a single item within a group.
    /// </summary>
    public sealed partial class ItemPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        DataBridge2 db;
        List<PoemItem> l = new List<PoemItem>();
        private string textTransition = "TLeft";
        Point swipeInitialPosition;
        Windows.UI.Input.GestureRecognizer gr = new Windows.UI.Input.GestureRecognizer();  


        public ItemPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;

            DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
            dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager,
            DataRequestedEventArgs>(this.ShareTextHandler);
        }



        private async void ShareTextHandler(DataTransferManager sender, DataRequestedEventArgs e)
        {
            DataRequest request = e.Request;
            // The Title is mandatory
            request.Data.Properties.Title = App.dataSource.getPoetNameOfAPoem(db.id) + ": " + db.title;
            request.Data.Properties.Description = "اشتراک‌گذاری این شعر با دیگران";
            string s = " \r\n \r\n";
            List<VerseItem> verses = new List<VerseItem>();
            await Task.Run(() => { verses = App.dataSource.GetVerses(db.id); });

            foreach (VerseItem item in verses)
            {
                s += item.hemistich1;
                if (item.hemistich2Visibility == Windows.UI.Xaml.Visibility.Visible)
                {
                    s += " / " + item.hemistich2;
                }
                s += " \r\n";
            }
            s += " \r\n \r\n";
            s += "توسط برنامه‌ی «گنجینه» برای ویندوز ۸.۱";
            request.Data.SetText(s);
            request.Data.SetHtmlFormat(HtmlFormatHelper.CreateHtmlFormat(s.Replace("\n","<br />")));
        }

        /// <summary>
        /// Gets the NavigationHelper used to aid in navigation and process lifetime management.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the DefaultViewModel. This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }



        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            string data;
            string versedata;

            db = Newtonsoft.Json.JsonConvert.DeserializeObject<DataBridge2>(e.NavigationParameter.ToString());

            if (db.HideNextPrevKeys)
            {
                NextButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                PrevButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }

            int maxfSize;
            int minfSize;
            string sF = "";
            string fontName = "empty";
            bool needsYeCorrection = false;
            if (App.DefaultFont != null)
            {
                sF = App.DefaultFont.Source.ToString();
            }

            if (sF.Contains("Nazanin"))
            {
                maxfSize = 34;
                minfSize = 28;
                fontName = "nazanin";
                needsYeCorrection = true;
            }
            else if (sF.Contains("Kamran"))
            {
                maxfSize = 38;
                minfSize = 30;
                fontName = "kamran";
                needsYeCorrection = true;
            }
            else if (sF.Contains("Nastaliq"))
            {
                maxfSize = 54;
                minfSize = 48;
                fontName = "nastaliq";
            }
            else
            {
                maxfSize = 30;
                minfSize = 22;
            }



            List<VerseItem2> verses = new List<VerseItem2>();
            await Task.Run(() => { verses = App.dataSource.GetVerses2(db.id); });
            versedata = GetVerseDataString(verses, textTransition);

            if (needsYeCorrection)
            {
                versedata = versedata.Replace("هٔ", "ة‌"); //Haz nim-faseleh after ة
            }

            data = ShowItemHTML.Replace("{2}", textTransition).Replace("{1}", versedata).Replace("{4}", fontName).Replace("{5}", maxfSize.ToString()).Replace("{7}", minfSize.ToString()).Replace("{6}", fontName == "empty" ? "'Segoe UI'" : "'TheFont'");
            if (App.SearchQuery.Length > 0)
            {
                data = data.Replace("{3}", "<script src='ms-appx-web:///Assets/jquery.highlight.js'></script>");
            }
            else
            {
                data = data.Replace("{3}", "");
            }
            pageTitle.Text = db.title;
            

            /* !!!!!!!!!!!!!!! FONT IS NOT APPLIED YET !!!!!!!!!!!!!!!!! */

            /**
            {
                byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(data.ToCharArray());
                try
                {
                    var sf = await ApplicationData.Current.LocalFolder.GetFileAsync("test.html");
                    await sf.DeleteAsync();
                }
                catch { }
                var storageFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("test.html");

                using (var f = await storageFile.OpenStreamForWriteAsync())
                {
                    f.Write(fileBytes, 0, fileBytes.Length);
                }
            }
            /**/
            WebView.NavigateToString(data);
            await Task.Run(() => { l = App.dataSource.GetListOfPoems(db.parentid); });





            var a = (from PoemItem p in App.favorites
                     where p.ID == db.id
                     select p).ToList();
            if (a.Count() > 0)
            {
                AppBarFavButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                AppBarUnFavButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else //required for next/prev keys
            {
                AppBarFavButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                AppBarUnFavButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }


            GoogleAnalytics.EasyTracker.GetTracker().SendView("ItemPage");
            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "Show", db.title + ", " + db.parentid, 0);

            // TODO: Create an appropriate data model for your problem domain to replace the sample data
            //WebView.NavigateToString(ShowItemHTML);
            // WebView.NavigateToLocalStreamUri("");
        }

        private static string GetVerseDataString(List<VerseItem2> verses, string transition)
        {
            if (verses.Count == 0)
            {
                return "";
            }
            string versedata;
            int maxPos = ((VerseItem2)verses.OrderByDescending(i => i.position).FirstOrDefault()).position;
            int minPos = ((VerseItem2)verses.OrderBy(i => i.position).FirstOrDefault()).position;
            
            //Fix some poems
            int maxPosCount = (from VerseItem2 v in verses
                              where v.position == maxPos
                              select v).ToList().Count;

            if (maxPosCount + 3 >= verses.Count)
            {
                for (int i = 0; i < verses.Count; i++)
                {
                    if (verses[i].position == maxPos)
                    {
                        verses[i].position = minPos;
                    }
                    else if (verses[i].position == minPos)
                    {
                        verses[i].position = minPos + 1;
                    }
                }
                maxPos = ((VerseItem2)verses.OrderByDescending(i => i.position).FirstOrDefault()).position;
            }


            
            versedata = "";
            if ((minPos == -1) && (maxPos == minPos)) //This is plain text, or line by line poet
            {
                foreach (VerseItem2 item in verses)
                {
                    versedata += VerseDIV.Replace("@1", "margin-right:0%").Replace("@2", item.text).Replace("@3", "width: 100%; text-align:right;");
                }
            }
            else if (minPos == -1) //Mixture of poet and text
            {
                int zareeb = 100 / (maxPos - minPos + 1 + 1);
                foreach (VerseItem2 item in verses)
                {
                    versedata += VerseDIV.Replace("@1", "margin-right:" + (item.position == -1 ? "0%" : (item.position * zareeb).ToString()) + "%").Replace("@2", item.text).Replace("@3", "width: " + (item.position == -1 ? "100%; text-align:right;" : "30%")).Replace("@4", "");
                }
            }
            else if (maxPos > 1) //This is new poet
            {
                
                int zareeb = 100 / (maxPos - minPos + 1);
                foreach (VerseItem2 item in verses)
                {
                    versedata += VerseDIV.Replace("@1", "margin-right:" + ((item.position - minPos) * zareeb).ToString() + "%").Replace("@2", item.text).Replace("@3", (item.position == minPos ? ("min-width:" + (zareeb > 30 ? zareeb.ToString() : "30") +"%") : "25%")).Replace("@4", "classicpoem rverse");
                }
            }
            else //This is classic poet
            {
                bool b = true;
                foreach (VerseItem2 item in verses)
                {
                    versedata += VerseDIV.Replace("@1", "").Replace("@2", item.text).Replace("@3", "").Replace("@4", "classicpoem " + (b ? "rverse" : "lverse"));
                    b = !b;
                }
            }
            return versedata;
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="Common.NavigationHelper.LoadState"/>
        /// and <see cref="Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </summary>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        #region const
        public const string ShowItemHTML =
"<html>                                                                                                                              " +
"<head>                                                                                                                              " +
"<script src='ms-appx-web:///Assets/jquery-2.1.1.min.js'></script>{3}                                                                " +
"<script src='ms-appx-web:///Assets/gesture.js'></script>                                                                            " +
"<link rel='stylesheet' type='text/css' href='ms-appx-web:///Assets/{4}.css'> " +
"<style>                                                                                                                             " +

"html {                                                                                                                              " +
"    direction: rtl;                                                                                                                 " +
"}                                                                                                                                   " +
"#container {                                                                                                                        " +
"    width:100%;                                                                                                                     " +
"	opacity:0;                                                                                                                       " +
"	transform:translate(0px);                                                                                                        " +
"   font-family:{6};                                                                                                                " +
"}                                                                                                                                   " +
".highlight {background:Gold;}                                                                                                       " +
".transition {                                                                                                                       " +
"    transition: transform 0.3s ease-out, opacity 0.3s linear;                                                                       " +
"}                                                                                                                                   " +
".GoToRight {                                                                                                                        " +
"    opacity:0;                                                                                                                      " +
"	transform:translate(100px);                                                                                                      " +
"}                                                                                                                                   " +
" @media (orientation : portrait) { .lverse {text-align:left} .rverse {text-align:right} #container {font-size:{7}px}                 " +
"                                    .classicpoem { width: 100%; margin-right:0%}  }                                                 " +
" @media (orientation : landscape) { .lverse {text-align:center} .rverse {text-align:center}  #container {font-size:{5}px}            " +
"                                    .lverse { width: 55%; margin-right:45%}  .rverse { width: 55%; margin-right:5%} }               " +
".GoToLeft {                                                                                                                         " +
"	opacity:0;                                                                                                                       " +
"	transform:translateX(-100px);                                                                                                    " +
"}                                                                                                                                   " +
"                                                                                                                                    " +
".TLeft {                                                                                                                            " +
"	transform:translateX(-100px);                                                                                                    " +
"}                                                                                                                                   " +
".TRight {                                                                                                                           " +
"	transform:translate(100px);                                                                                                      " +
"}                                                                                                                                   " +
".show {                                                                                                                             " +
"	opacity:1 !important;                                                                                                            " +
"}                                                                                                                                   " +
"</style>                                                                                                                            " +
"<script>                                                                                                                            " +
"                                                                                                                                    " +
"	$(function(){                                                                                                                    " +
"	t = $('div');                                                                                                                    " +
"	t.addClass('{2}');                                                                                                               " +
"	t.addClass('transition');                                                                                                        " +
"	setTimeout(function() {                                                                                                          " +
"		  t.addClass('show');                                                                                                        " +
"		  t.removeClass('TLeft');                                                                                                    " +
"		  t.removeClass('TRight');                                                                                                   " +
"	}, 100);                                                                                                                         " +
"});                                                                                                                                 " +
"	function goToLeft() {                                                                                                            " +
"		t.addClass('GoToLeft').removeClass('show');                                                                                  " +
"	}                                                                                                                                " +
"	function goToRight() {                                                                                                           " +
"		t.addClass('GoToRight').removeClass('show');                                                                                 " +
"	}                                                                                                                                " +
"   function hl(s) {                                                                                                                 " +
"       $('#container').highlight(s); }                                                                                               " +
"</script>                                                                                                                           " +
"</head>                                                                                                                             " +
"<body>                                                                                                                              " +
"<div id='container'>{1}                                                       " +
"</div>                                                                                                                              " +
"</body>                                                                                                                             " +
"</html>                                                                                                                             ";

        public const string VerseDIV = "<div style='@1; @3' class='@4'>@2</div>";

        #endregion


        private async void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            List<string> s = new List<string>();

            await WebView.InvokeScriptAsync("goToLeft", s);

            if (l.Count == 0) { return; }
            int j = 0;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].ID == db.id)
                {
                    j = i;
                    break;
                }
            }

            j -= 1;
            if (j < 0)
            {
                j = l.Count - 1;
            }
            DataBridge2 dba = new DataBridge2()
            {
                parentid = db.parentid,
                id = l[j].ID,
                title = l[j].Text
            };
            LoadStateEventArgs ee = new LoadStateEventArgs(Newtonsoft.Json.JsonConvert.SerializeObject(dba), new Dictionary<string, object>());
            textTransition = "TRight";
            await Task.Delay(TimeSpan.FromMilliseconds(200));
            NavigationHelper_LoadState(this, ee);
        }

        private async void NextButton_Click(object sender, RoutedEventArgs e)
        {
            List<string> s = new List<string>();

            await WebView.InvokeScriptAsync("goToRight", s);

            if (l.Count == 0) { return; }
            int j = l.Count - 1;
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].ID == db.id)
                {
                    j = i;
                    break;
                }
            }

            j += 1;
            if (j >= l.Count)
            {
                j = 0;
            }
            DataBridge2 dba = new DataBridge2()
            {
                parentid = db.parentid,
                id = l[j].ID,
                title = l[j].Text
            };
            LoadStateEventArgs ee = new LoadStateEventArgs(Newtonsoft.Json.JsonConvert.SerializeObject(dba), new Dictionary<string, object>());
            textTransition = "TLeft";
            await Task.Delay(TimeSpan.FromMilliseconds(200));
            NavigationHelper_LoadState(this, ee);
        }

        private void WebView_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            if (args.Uri != null)
            {
                if (args.Uri.OriginalString != "ms-appx-web:///")
                {
                    args.Cancel = true;
                    string s = args.Uri.ToString();
                    if (s.Contains("prev"))
                    {
                        PrevButton_Click(sender, new RoutedEventArgs());
                    }
                    else if (s.Contains("next"))
                    {
                        NextButton_Click(sender, new RoutedEventArgs());
                    }
                }
            }

        }

        private void WebView_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            WebView.Visibility = Windows.UI.Xaml.Visibility.Visible;
            if (App.SearchQuery.Length > 0)
            {
                List<string> s = new List<string>();
                s.Add(App.SearchQuery);
                WebView.InvokeScriptAsync("hl", s);
            }
        }

        private void pageRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize.Width > e.NewSize.Height) //Landscape
            {
                WebView.Margin = new Thickness(175, 10, 175, 10);
            }
            else //Portrait
            {
                WebView.Margin = new Thickness(20, 10, 20, 10);
            }
        }

        private void AppBarFavButton_Click(object sender, RoutedEventArgs e)
        {
            App.dataSource.AddToFavorites(db.id);
            AppBarUnFavButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            AppBarFavButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "Like", db.title + "، " + db.parentid, db.id);
        }

        private void AppBarUnFavButton_Click(object sender, RoutedEventArgs e)
        {
            App.dataSource.RemoveFromFavorites(db.id);
            AppBarUnFavButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            AppBarFavButton.Visibility = Windows.UI.Xaml.Visibility.Visible;

            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "Unlike", db.title + "، " + db.parentid, db.id);
        }


        internal async void RefreshPage()
        {
            List<string> s = new List<string>();
            await WebView.InvokeScriptAsync("goToLeft", s);
            LoadStateEventArgs ee = new LoadStateEventArgs(Newtonsoft.Json.JsonConvert.SerializeObject(db), new Dictionary<string, object>());
            textTransition = "TRight";
            await Task.Delay(TimeSpan.FromMilliseconds(200));
            NavigationHelper_LoadState(this, ee);
        }

        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appBar.IsOpen = true;
        }

        private void pageRoot_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("PointerPressed");
        }

        private void pageRoot_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("PointerMoved");
        }

        private void pageRoot_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("PointerReleased");
        }
    }

}
