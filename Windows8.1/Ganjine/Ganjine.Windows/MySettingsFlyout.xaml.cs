﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Settings Flyout item template is documented at http://go.microsoft.com/fwlink/?LinkId=273769

namespace Ganjine
{
    public sealed partial class MySettingsFlyout : SettingsFlyout
    {
        bool Initialized = false;
        public MySettingsFlyout()
        {
            this.InitializeComponent();

            GoogleAnalytics.EasyTracker.GetTracker().SendView("SettingsPage");
        }

        private void SettingsFlyout_Loaded(object sender, RoutedEventArgs e)
        {
            if (App.DefaultFont == null)
            {
                Font1.IsChecked = true;
            }
            else if (App.DefaultFont.Source.Contains("Nazanin"))
            {
                Font2.IsChecked = true;
            }
            else if (App.DefaultFont.Source.Contains("Kamran"))
            {
                Font3.IsChecked = true;
            }
            else if (App.DefaultFont.Source.Contains("IranNastaliq"))
            {
                Font4.IsChecked = true;
            }
            else //Some misconfiguration
            {
                Font1.IsChecked = true;
            }
            Initialized = true;
        }

        private void SaveSettings()
        {
            if (Font1.IsChecked == true)
            {
                App.DefaultFont = null;
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "FontFlyoutName", Font1.Content.ToString(), 0);
            }
            else if (Font2.IsChecked == true)
            {
                App.DefaultFont = (FontFamily)App.Current.Resources["NazaninFontFamily"];
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "FontFlyoutName", Font2.Content.ToString(), 0);
            }
            else if (Font3.IsChecked == true)
            {
                App.DefaultFont = (FontFamily)App.Current.Resources["KamranFontFamily"];
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "FontFlyoutName", Font3.Content.ToString(), 0);
            }
            else if (Font4.IsChecked == true)
            {
                App.DefaultFont = (FontFamily)App.Current.Resources["NastaliqFontFamily"];
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "FontFlyoutName", Font4.Content.ToString(), 0);

            }

            Page hp = (Page)((Frame)Window.Current.Content).Content;
            if (hp.GetType().FullName == "Ganjine.ItemPage")
            {
                ((ItemPage)hp).RefreshPage();
            }

            App.dataSource.SaveSettings();
        }

        private void Font_Checked(object sender, RoutedEventArgs e)
        {
            if (Initialized)
            {
                SaveSettings();
            }
        }

        private void SettingsFlyout_BackClick(object sender, BackClickEventArgs e)
        {
            this.Hide();
        }
    }
}