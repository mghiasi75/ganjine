﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Ganjine.DataModel;
using Ganjine.Common;
using SQLitePCL;
using Windows.UI.Popups;
using Windows.Storage;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.DataTransfer;
using System.ServiceModel.Channels;
using System.Collections.ObjectModel;

// The Universal Hub Application project template is documented at http://go.microsoft.com/fwlink/?LinkID=391955

namespace Ganjine
{
    /// <summary>
    /// A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class HubPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private TextBlock NoFavorites;
        private ListView FavoritesList;
        /// <summary>
        /// Gets the NavigationHelper used to aid in navigation and process lifetime management.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the DefaultViewModel. This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        public HubPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            // TODO: Create an appropriate data model for your problem domain to replace the sample data
            
        }

        /// <summary>
        /// Invoked when a HubSection header is clicked.
        /// </summary>
        /// <param name="sender">The Hub that contains the HubSection whose header was clicked.</param>
        /// <param name="e">Event data that describes how the click was initiated.</param>
        void Hub_SectionHeaderClick(object sender, HubSectionHeaderClickEventArgs e)
        {
            /*HubSection section = e.Section;
            var group = section.DataContext;
            this.Frame.Navigate(typeof(SectionPage), ((SampleDataGroup)group).UniqueId);*/
        }

        /// <summary>
        /// Invoked when an item within a section is clicked.
        /// </summary>
        /// <param name="sender">The GridView or ListView
        /// displaying the item clicked.</param>
        /// <param name="e">Event data that describes the item clicked.</param>
        void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Navigate to the appropriate destination page, configuring the new page
            // by passing required information as a navigation parameter
            /*var itemId = ((SampleDataItem)e.ClickedItem).UniqueId;
            this.Frame.Navigate(typeof(ItemPage), itemId);*/
        }





        private async Task CopyDatabaseAsync()
        {
            bool versionMismatch = false;
            bool isDatabaseExisting = false;
            StorageFile storageFile = null;
            try
            {
                storageFile = await ApplicationData.Current.LocalFolder.GetFileAsync("ganjoor2.s3db");
                isDatabaseExisting = true;
            }
            catch
            {
                initProgress.Visibility = Windows.UI.Xaml.Visibility.Visible;
                //Hub.IsEnabled = false;
                isDatabaseExisting = false;
            }
            try
            {
                StorageFile versionFile = await ApplicationData.Current.LocalFolder.GetFileAsync("version.txt");
                StorageFile currentVersionFile = await Package.Current.InstalledLocation.GetFileAsync("version.txt");
                double version = double.Parse(await Windows.Storage.FileIO.ReadTextAsync(versionFile));
                double currentVersion = double.Parse(await Windows.Storage.FileIO.ReadTextAsync(currentVersionFile));
                if (version != currentVersion)
                {
                    versionMismatch = true;
                }
            }
            catch
            {
                initProgress.Visibility = Windows.UI.Xaml.Visibility.Visible;

                versionMismatch = true;
            }
            if (versionMismatch)
            {
                StorageFile currentVersionFile = await Package.Current.InstalledLocation.GetFileAsync("version.txt");

                //Delete old version file, if exists.
                try
                {
                    StorageFile vFile = await ApplicationData.Current.LocalFolder.GetFileAsync("version.txt");
                    await vFile.DeleteAsync();
                }
                catch
                {

                }

                await currentVersionFile.CopyAsync(ApplicationData.Current.LocalFolder);
                if (isDatabaseExisting == true)
                {
                    initProgress.Visibility = Windows.UI.Xaml.Visibility.Visible;
                      
                    await storageFile.DeleteAsync(StorageDeleteOption.PermanentDelete);
                    isDatabaseExisting = false;
                }
            }

            if (!isDatabaseExisting)
            {
                StorageFile databaseFile = await Package.Current.InstalledLocation.GetFileAsync("ganjoor2.s3db");
                await databaseFile.CopyAsync(ApplicationData.Current.LocalFolder, "ganjoor2.s3db", NameCollisionOption.ReplaceExisting);
            }
        }


        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="Common.NavigationHelper.LoadState"/>
        /// and <see cref="Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </summary>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            /*using (var statement = App.conn.Prepare("SELECT name FROM poet"))
            {
                if (SQLiteResult.DONE == statement.Step())
                {
                    var dialog = new MessageDialog(statement[0].ToString());
                    await dialog.ShowAsync();
                }
            }*/


            await CopyDatabaseAsync();

            //Create connection with database
            App.dataSource.CheckDBConnection();

            if (!this.defaultViewModel.ContainsKey("Poets"))
            {
                //Read list of poets.
                ObservableCollection<PoetItem> p1 = App.dataSource.GetListOfPoets();
                this.defaultViewModel["Poets"] = p1;


                List<PoetItem> pp = new List<PoetItem>();
                pp.Add(new PoetItem() { PoetID = -10, PoetName = "" });
                pp.Add(new PoetItem() { PoetID = -1, PoetName = "همه‌ی آثار" });
                pp.AddRange(p1);

                this.defaultViewModel["Soruces"] = pp;

                /* !!!    try { searchSource.SelectedIndex = 0; }
                    catch { } */

                await App.dataSource.InitializeSettings();


            }



            await App.dataSource.InitializeFavorites();
            List<PoemItem> ppa = new List<PoemItem>();
            ppa.AddRange(App.favorites);
            this.defaultViewModel["Favs"] = ppa;

            try
            {
                if (ppa.Count == 0)
                {
                    NoFavorites.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
                else
                {
                    NoFavorites.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                }
            }
            catch { }

            initProgress.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            GoogleAnalytics.EasyTracker.GetTracker().SendView("MainPage");

            this.navigationHelper.OnNavigatedTo(e);


        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void FavItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            PoemItem pi = (PoemItem)e.ClickedItem;
            DataBridge2 db = new DataBridge2()
            {
                parentid = 0,
                id = pi.ID,
                title = pi.Text,
                HideNextPrevKeys = true
            };
            Frame.Navigate(typeof(ItemPage), Newtonsoft.Json.JsonConvert.SerializeObject(db));
        }

        private void SearchButton_Tapped(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SearchResultsPage), SearchBox.Text);
        }

        private void SearchBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                SearchButton_Tapped(sender, new RoutedEventArgs());
            }
        }

/*        private void pageRoot_KeyUp(object sender, KeyRoutedEventArgs e)
        {

           
            if (SearchBox.Text.Length > 0) { return; }   
            string s = e.Key.ToString();
            if (s.Length == 1)
            {
                SearchBox.Focus(FocusState.Keyboard);
                e.Handled = false;
            //    SearchBox.Text = s;
            }
        }
    */

        private async void itemGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var poet = (PoetItem)(e.ClickedItem);
            DataBridge1 db = new DataBridge1()
            {
                id = poet.CatID,
                title = poet.PoetName
            };
            Frame.Navigate(typeof(SectionPage), Newtonsoft.Json.JsonConvert.SerializeObject(db));
        }

        private void TextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            NoFavorites = (TextBlock)sender;
        }

        private void ListView_Loaded(object sender, RoutedEventArgs e)
        {
            FavoritesList = (ListView)sender;
        }

        private void pageRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize.Width < 600)
            {
                pageTitle.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                FavoritesHub.Width = 275;
                SearchGrid.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
                SearchGrid.Margin = new Thickness(30, -30, 0, 0);
                SearchBox.Width = 180;
            }
            else
            {
                pageTitle.Visibility = Windows.UI.Xaml.Visibility.Visible;
                FavoritesHub.Width = 400;
                SearchGrid.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
                SearchGrid.Margin = new Thickness(0, 0, 0, 0);
                SearchBox.Width = 220;
            }
        }

        private void pageRoot_LayoutUpdated(object sender, object e)
        {
            /*Windows.ApplicationModel.DataTransfer.DataPackage dp = new Windows.ApplicationModel.DataTransfer.DataPackage();
            dp.SetText("HELLO!! TESTTTTT");
            Windows.ApplicationModel.DataTransfer.Clipboard.SetContent(dp);*/
        }


        private void AppBarSettingsButton_Click(object sender, RoutedEventArgs e)
        {
            MySettingsFlyout f = new MySettingsFlyout();
            f.ShowIndependent();
        }

        private void AppBarAboutButton_Click(object sender, RoutedEventArgs e)
        {
            MyAboutFlyout f = new MyAboutFlyout();
            f.ShowIndependent();
        }

        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appBar.IsOpen = true;
        }

    private void KeyPress(Windows.UI.Core.CoreWindow sender, Windows.UI.Core.CharacterReceivedEventArgs args)
    {
        if (SearchBox.FocusState == Windows.UI.Xaml.FocusState.Unfocused)
        {
            SearchBox.Text = Convert.ToChar(args.KeyCode).ToString();
            SearchBox.SelectionStart = SearchBox.Text.Length;
            SearchBox.SelectionLength = 0;
            SearchBox.Focus(FocusState.Programmatic);

        }
    }

    private void pageRoot_GotFocus(object sender, RoutedEventArgs e)
    {
        Window.Current.CoreWindow.CharacterReceived += KeyPress;
    }

    private void pageRoot_LostFocus(object sender, RoutedEventArgs e)
    {
        Window.Current.CoreWindow.CharacterReceived -= KeyPress;
    }




    }

}
