﻿using Ganjine.DataModel;
using Ganjine.Common;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Threading.Tasks;

namespace Ganjine
{
    /// <summary>
    /// A page that displays an overview of a single group, including a preview of the items
    /// within the group.
    /// </summary>
    public sealed partial class SectionPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private int myid;

        public SectionPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
        }

        /// <summary>
        /// Gets the NavigationHelper used to aid in navigation and process lifetime management.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the DefaultViewModel. This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            DataBridge1 db = Newtonsoft.Json.JsonConvert.DeserializeObject<DataBridge1>(e.NavigationParameter.ToString());
            defaultViewModel["Title"] = db.title;
            List<PoemItem> l = new List<PoemItem>();
            int i = 0;

            //Avoid hanging UI while we're loading from database.
            await Task.Run(() =>
            {
                l.AddRange(App.dataSource.GetListOfChildren(db.id));
                List<PoemItem> b = App.dataSource.GetListOfPoems(db.id);
                l.AddRange(b);
                i = b.Count;
            });



            myid = db.id;
            defaultViewModel["Items"] = l;
            if (i > 10)
            {
                appBar.Visibility = Visibility.Visible;
            }

        }

        /// <summary>
        /// Invoked when an item is clicked.
        /// </summary>
        /// <param name="sender">The GridView displaying the item clicked.</param>
        /// <param name="e">Event data that describes the item clicked.</param>
        private void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            ItemClick(e.ClickedItem);
        }

        private void ItemClick(object c)
        {
            PoemItem poem = (PoemItem)(c);

            if (poem.Type == PoemItemType.Category)
            {
                DataBridge1 db = new DataBridge1()
                {
                    id = poem.ID,
                    title = poem.Text
                };
                Frame.Navigate(typeof(SectionPage), Newtonsoft.Json.JsonConvert.SerializeObject(db));
            }
            else
            {
                DataBridge2 db = new DataBridge2()
                {
                    parentid = myid,
                    id = poem.ID,
                    title = poem.Text
                };
                Frame.Navigate(typeof(ItemPage), Newtonsoft.Json.JsonConvert.SerializeObject(db));

            }


            /* var itemId = ((SampleDataItem)e.ClickedItem).UniqueId;
             if (!Frame.Navigate(typeof(ItemPage), itemId))
             {
                 var resourceLoader = ResourceLoader.GetForCurrentView("Resources");
                 throw new Exception(resourceLoader.GetString("NavigationFailedExceptionMessage"));
             }*/
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="Common.NavigationHelper.LoadState"/>
        /// and <see cref="Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </summary>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void AppBarRandomButton_Click(object sender, RoutedEventArgs e)
        {
            Random r = new Random();
            ItemClick(itemGridView.Items[r.Next(0, itemGridView.Items.Count - 1)]);
        }

        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            appBar.IsOpen = true;
        }
    }
}