﻿using Ganjine.Common;
using Ganjine.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// TODO: Connect the Search Results Page to your in-app search.
// The Search Results Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234240

namespace Ganjine
{
    /// <summary>
    /// This page displays search results when a global search is directed to this application.
    /// </summary>
    public sealed partial class SearchResultsPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private bool CancelSearch;
        List<PoetItem> poetsList = new List<PoetItem>();
        string QueryText;
        private bool isSearching;

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }
        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public SearchResultsPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
        }


        private async Task SearchInsideAPoet(string s, int i, int l)
        {
            DateTime beginSearch = DateTime.Now;

            List<PoemItem> pi = new List<PoemItem>();
            //Stopwatch ss = new Stopwatch();

            await Windows.System.Threading.ThreadPool.RunAsync(_ =>
            {
                //ss.Start();    
                //pi.AddRange((List<PoemItem>)this.defaultViewModel["SearchResults"]);
                pi.AddRange(App.dataSource.Search(s, i, l, AdvancedSearchMode.Phrase));


                //ss.Stop();
            }, Windows.System.Threading.WorkItemPriority.Normal, Windows.System.Threading.WorkItemOptions.None);
            /*var dialog = new MessageDialog("with await: " + ss.ElapsedMilliseconds.ToString() + "ms");
            await dialog.ShowAsync();*/

            foreach (PoemItem item in pi)
            {
                resultsGridView.Items.Add(item);
            }

            GoogleAnalytics.EasyTracker.GetTracker().SendTiming(DateTime.Now.Subtract(beginSearch), "Search", App.dataSource.GetPoetName(i), s);
        }


        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            QueryText = e.NavigationParameter as String;

            var myTracker = GoogleAnalytics.EasyTracker.GetTracker();
            myTracker.SendView("SearchPage");

            // TODO: Application-specific searching logic.  The search process is responsible for
            //       creating a list of user-selectable result categories:
            //
            //       filterList.Add(new Filter("<filter name>", <result count>));
            //
            //       Only the first filter, typically "All", should pass true as a third argument in
            //       order to start in an active state.  Results for the active filter are provided
            //       in Filter_SelectionChanged below.

            var filterList = new List<Filter>();
            filterList.Add(new Filter("All", 0, true));

            // Communicate results through the view model
            this.DefaultViewModel["QueryText"] = '«' + QueryText + '»';
            this.DefaultViewModel["Filters"] = filterList;
            this.DefaultViewModel["ShowFilters"] = filterList.Count > 1;

            //Windows.UI.ViewManagement.StatusBar.GetForCurrentView().BackgroundColor = Windows.UI.Color.FromArgb(255,3,255,3);



            //Read list of poets.


            
            poetsList.Add(new PoetItem() { PoetID = -1, PoetName = "همه‌ی آثار" });
            poetsList.AddRange(App.dataSource.GetListOfPoets());
            this.defaultViewModel["Poets"] = poetsList;

            
            await DoSearch(QueryText);
            
        }
    
        private async Task DoSearch(string queryText)
        {
            isSearching = true;
            App.SearchQuery = queryText;


            //In order to constantly updating listView, we have to add Items manually, so we don't use ViewModel :)

            CancelSearch = false;
            NoResults.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            string s = queryText;
            int i;
            try { i = ((PoetItem)searchSource.SelectedItem).PoetID; }
            catch { i = -1; }
            App.SearchQuery = s;
            resultsGridView.Items.Clear();

            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Search", i == -1 ? "همه‌ی آثار" : ((PoetItem)searchSource.SelectedItem).PoetName, queryText, 0);

            if (i > 0)
            {
                await SearchInsideAPoet(s, i, 500);
            }
            else
            {
                searchProgressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
                for (int x = 0; x < poetsList.Count; x++)
                {
                    PoetItem pi = ((PoetItem)poetsList[x]);
                    await SearchInsideAPoet(s, pi.PoetID, 100);
                    if (CancelSearch)
                    {
                        break;
                    }
                }
                searchProgressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }

            

            if ((resultsGridView.Items.Count == 0) && (!CancelSearch))
            {
                NoResults.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }

            isSearching = false;
        }


        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        /// <summary>
        /// View model describing one of the filters available for viewing search results.
        /// </summary>
        private sealed class Filter : INotifyPropertyChanged
        {
            private String _name;
            private int _count;
            private bool _active;

            public Filter(String name, int count, bool active = false)
            {
                this.Name = name;
                this.Count = count;
                this.Active = active;
            }

            public override String ToString()
            {
                return Description;
            }

            public String Name
            {
                get { return _name; }
                set { if (this.SetProperty(ref _name, value)) this.OnPropertyChanged("Description"); }
            }

            public int Count
            {
                get { return _count; }
                set { if (this.SetProperty(ref _count, value)) this.OnPropertyChanged("Description"); }
            }

            public bool Active
            {
                get { return _active; }
                set { this.SetProperty(ref _active, value); }
            }

            public String Description
            {
                get { return String.Format("{0} ({1})", _name, _count); }
            }

            /// <summary>
            /// Multicast event for property change notifications.
            /// </summary>
            public event PropertyChangedEventHandler PropertyChanged;

            /// <summary>
            /// Checks if a property already matches a desired value.  Sets the property and
            /// notifies listeners only when necessary.
            /// </summary>
            /// <typeparam name="T">Type of the property.</typeparam>
            /// <param name="storage">Reference to a property with both getter and setter.</param>
            /// <param name="value">Desired value for the property.</param>
            /// <param name="propertyName">Name of the property used to notify listeners.  This
            /// value is optional and can be provided automatically when invoked from compilers that
            /// support CallerMemberName.</param>
            /// <returns>True if the value was changed, false if the existing value matched the
            /// desired value.</returns>
            private bool SetProperty<T>(ref T storage, T value, [CallerMemberName] String propertyName = null)
            {
                if (object.Equals(storage, value)) return false;

                storage = value;
                this.OnPropertyChanged(propertyName);
                return true;
            }

            /// <summary>
            /// Notifies listeners that a property value has changed.
            /// </summary>
            /// <param name="propertyName">Name of the property used to notify listeners.  This
            /// value is optional and can be provided automatically when invoked from compilers
            /// that support <see cref="CallerMemberNameAttribute"/>.</param>
            private void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                var eventHandler = this.PropertyChanged;
                if (eventHandler != null)
                {
                    eventHandler(this, new PropertyChangedEventArgs(propertyName));
                }
            }

        }

    
        private void searchSource_Loaded(object sender, RoutedEventArgs e)
        {
            searchSource.SelectedIndex = 0;
        }

        private async void searchSource_ItemClick(object sender, ItemClickEventArgs e)
        {
            searchSource.SelectedItem = e.ClickedItem;
            CancelSearch = true;
            //wait until currentSearch finishes.
            while (isSearching)
            {
                await Task.Delay(TimeSpan.FromMilliseconds(100));
            }
            await DoSearch(QueryText);
        }

        private void resultsGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            PoemItem pi = (PoemItem)e.ClickedItem;
            DataBridge2 db = new DataBridge2()
            {
                parentid = 0,
                id = pi.ID,
                title = pi.Text,
                HideNextPrevKeys = true
            };
            Frame.Navigate(typeof(ItemPage), Newtonsoft.Json.JsonConvert.SerializeObject(db));
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            CancelSearch = true;
        }
    }
}
