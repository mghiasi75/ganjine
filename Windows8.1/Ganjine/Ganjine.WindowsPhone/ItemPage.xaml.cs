﻿using Ganjine.Common;
using Ganjine.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Universal Hub Application project template is documented at http://go.microsoft.com/fwlink/?LinkID=391955

namespace Ganjine
{
    /// <summary>
    /// A page that displays details for a single item within a group.
    /// </summary>
    public sealed partial class ItemPage : Page
    {
        private readonly NavigationHelper navigationHelper;
        private readonly ObservableDictionary defaultViewModel = new ObservableDictionary();

        List<PoemItem> listOfPoems = new List<PoemItem>();
        int currentPoemIndex = 0;
        string currentVerse = "";
        const int LoadedItemsAfterCurrent = 20;
        const int LoadedItemsBeforeCurrent = 10;
        const int CompletelyLoadedItemsAfterCurrent = 10;
        const int CompletelyLoadedItemsBeforeCurrent = 5;
        bool InitialPivotLoading = true;
        bool PivotLoading = false;
        int firstSearchItem = 0;

        private class PoemPivotItemData
        {
            public string Header { get; set; }
            public List<VerseItem> PoemItems { get; set; }
            public int PoemIndex { get; set; }
            public double ScrollPosition { get; set; }
        }

        private ObservableCollection<PoemPivotItemData> PivotPages = new ObservableCollection<PoemPivotItemData>();

        public ItemPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            int theme = (int)Windows.Storage.ApplicationData.Current.LocalSettings.Values["currentTheme"];
            if ((theme == 1) || (theme == 3))
                OverlayRectangle.Fill = new SolidColorBrush(Windows.UI.Colors.Black);
            else
                OverlayRectangle.Fill = new SolidColorBrush(Windows.UI.Colors.White);
        }


        private void ShareTextHandler(DataTransferManager sender, DataRequestedEventArgs e)
        {
            if (currentVerse.Length != 0) //Share a single verse
            {
                DataRequest request = e.Request;
                request.Data.Properties.Title = " ";
                request.Data.Properties.Description = "اشتراک‌گذاری";
                request.Data.SetText(currentVerse.Replace("ة‌", "هٔ")); //Haz nim-faseleh after ة
            }
            else
            {
                DataRequest request = e.Request;
                // The Title is mandatory
                if (listOfPoems.Count == 1) //This is from favorites, so it has poet name inside it.
                {
                    request.Data.Properties.Title = listOfPoems[0].Text;
                }
                else
                {
                    request.Data.Properties.Title = App.dataSource.getPoetNameOfAPoem(listOfPoems[currentPoemIndex].ID) + ": " + listOfPoems[currentPoemIndex].Text;
                }

                request.Data.Properties.Description = "اشتراک‌گذاری این شعر با دیگران";
                string s = " \r\n \r\n";
                List<VerseItem> verses;

                QKit.MultiSelectListView lv = GetVisualChild<QKit.MultiSelectListView>(ItemsPivot.ItemContainerGenerator.ContainerFromIndex(ItemsPivot.SelectedIndex));
                //await Task.Run(() => { verses = App.dataSource.GetVerses(listOfPoems[currentPoemIndex].ID); });

                if (lv.SelectionMode == ListViewSelectionMode.None) //Share all verses
                    verses = (from VerseItem item in lv.Items
                              select item).ToList();
                else //Share selected verses
                    verses = (from VerseItem item in lv.Items
                              where lv.SelectedItems.Contains(item)
                              select item).ToList();

                foreach (VerseItem item in verses)
                {
                    s += item.hemistich1;
                    if (item.hemistich2Visibility == Windows.UI.Xaml.Visibility.Visible)
                    {
                        s += " / " + item.hemistich2;
                    }
                    s += " \r\n";
                }
                s += " \r\n \r\n";
                s += "توسط برنامه‌ی «گنجینه» برای ویندوز فون";
                request.Data.SetText(s);
            }

        }



        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>. This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            AppBarSelectButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            AppBarShareButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            AppBarFavButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            AppBarUnFavButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            App.CurPageArg = "";
            if (App.ItemPageInitializeData == null)
            {
                try
                {
                    App.ItemPageInitializeData = Newtonsoft.Json.JsonConvert.DeserializeObject<DataBridge2>(e.NavigationParameter.ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception("Error while expanding DataBridge2 data. Here's the data: " + e.NavigationParameter.ToString(), ex);
                }
            }

            if (App.ItemPageInitializeData.parentid == 0) //Single item, from search or favorites.
            {
                listOfPoems.Add(App.dataSource.GetPoem(App.ItemPageInitializeData.id));
                currentPoemIndex = 0;
            }
            else
            {
                if (listOfPoems.Count == 0)
                    await Task.Run(() =>
                    {
                        listOfPoems = App.dataSource.GetListOfPoems(App.ItemPageInitializeData.parentid);
                    });

                for (int i = 0; i < listOfPoems.Count; i++)
                    if (listOfPoems[i].ID == App.ItemPageInitializeData.id)
                        currentPoemIndex = i;

                /* Restore correct pivot page if application is coming from suspension */
                int vitemid = -1;
                if (Windows.Storage.ApplicationData.Current.LocalSettings.Values["ViewedItemID"] != null)
                    if (int.TryParse(Windows.Storage.ApplicationData.Current.LocalSettings.Values["ViewedItemID"].ToString(), out vitemid))
                    {
                        if (vitemid > 0)
                        {
                            currentPoemIndex /*+*/= vitemid;
                            /*  if (currentPoemIndex >= listOfPoems.Count)
                                  currentPoemIndex -= listOfPoems.Count;*/
                        }
                    }
            }

            if (listOfPoems.Count == 1)
            {
                //We don't have animation in this condition.
                LoadPivotItem(currentPoemIndex, true, true, false);
                ItemsPivot.IsLocked = true;
            }
            else if (listOfPoems.Count <= (LoadedItemsAfterCurrent + LoadedItemsBeforeCurrent + 1)) //Virtualization is not necessary
            {
                //Load these two first, so we can see correct animation 
                LoadPivotItem(currentPoemIndex == 0 ? (listOfPoems.Count - 1) : (currentPoemIndex - 1), true, false, false);
                LoadPivotItem(currentPoemIndex, true, true, false);

                for (int i = currentPoemIndex + 1; i < listOfPoems.Count; i++)
                    LoadPivotItem(i, true, true, false);
                for (int i = 0; i < currentPoemIndex - 1; i++)
                    LoadPivotItem(i, true, true, false);

                /*for (int i = currentPoemIndex; i < listOfPoems.Count; i++)
                    LoadPivotItem(i, true, true);
                for (int i = 0; i < currentPoemIndex; i++)
                    LoadPivotItem(i, true, true);*/
            }
            else
            {
                for (int i = currentPoemIndex - LoadedItemsBeforeCurrent; i <= currentPoemIndex + LoadedItemsAfterCurrent; i++)
                {
                    LoadPivotItem(i, true, (i >= (currentPoemIndex - CompletelyLoadedItemsBeforeCurrent)) && (i <= (currentPoemIndex + CompletelyLoadedItemsAfterCurrent)), (i == (currentPoemIndex - LoadedItemsBeforeCurrent)) || (i == (currentPoemIndex + LoadedItemsBeforeCurrent)));
                }
                /*for (int i = currentPoemIndex + 1; i < listOfPoems.Count; i++)
                    LoadPivotItem(i, true, (i >= (currentPoemIndex - 1)) && (i <= (currentPoemIndex + 1)));
                for (int i = 0; i < currentPoemIndex - 1; i++)
                    LoadPivotItem(i, true, (i >= (currentPoemIndex - 1)) && (i <= (currentPoemIndex + 1)));*/
            }
            this.defaultViewModel["Pages"] = PivotPages;
            ItemsPivot.UpdateLayout();

            //SetScrollPosition();



            Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                InitialPivotLoading = true;
                await Task.Delay(20);
                if (listOfPoems.Count == 1)
                    ItemsPivot.SelectedIndex = 0;
                else if (listOfPoems.Count <= (LoadedItemsAfterCurrent + LoadedItemsBeforeCurrent + 1)) //Virtualization is not necessary
                {
                    ItemsPivot.SelectedIndex = 1;
                    await Task.Delay(100);
                    LoadVersesToPage(-1);
                }
                else
                    ItemsPivot.SelectedIndex = LoadedItemsBeforeCurrent;
                await Task.Delay(100);
                InitialPivotLoading = false;
                AppBarSelectButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                AppBarShareButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                DeterminateFavButtonVisibility();
                OverlayRectangle.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            });
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["ViewedItemID"] = currentPoemIndex;
            //App.ViewedItemID = currentPoemIndex;
            /*
            try
            {
                if (Frame.BackStack[Frame.BackStackDepth - 1].SourcePageType == typeof(ItemPage))
                {
                    Frame.BackStack.RemoveAt(Frame.BackStackDepth - 1);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while cleaning BackStack up.", ex);
            }
            */
            /*
                        try
                        {
                            await Task.Run(() => { l = App.dataSource.GetListOfPoems(db.parentid); });
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error while getting list of poems.", ex);
                        }
                        */
            try
            {
                DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
                dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager,
                DataRequestedEventArgs>(this.ShareTextHandler);
            }
            catch (Exception ex)
            {
                throw new Exception("Error while contacting share handler.", ex);
            }

            // TODO: Create an appropriate data model for your problem domain to replace the sample data
            /*   var item = await SampleDataSource.GetItemAsync((string)e.NavigationParameter);
               this.DefaultViewModel["Item"] = item;*/


            DispatcherTimer tmr = new DispatcherTimer();

            tmr.Interval = TimeSpan.FromSeconds(1);

            tmr.Tick += (object s, object ee) =>
            {
                ScrollViewer sv = GetVisualChild<ScrollViewer>(ItemsPivot.ItemContainerGenerator.ContainerFromIndex(ItemsPivot.SelectedIndex));
                if ((sv != null) && (App.ItemPageInitializeData != null))
                {
                    var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                    localSettings.Values["scrollPos" + listOfPoems[currentPoemIndex].ID] = sv.VerticalOffset;
                    //System.Diagnostics.Debug.WriteLine(sv.VerticalOffset);
                }
                else if (App.ItemPageInitializeData == null)
                {
                    tmr.Stop();
                }
            };

            tmr.Start();
        }

        public T GetVisualChild<T>(DependencyObject parent) where T : DependencyObject
        {
            if (parent == null)
                return null;
            T child = default(T);
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                DependencyObject v = (DependencyObject)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                    child = GetVisualChild<T>(v);
                if (child != null)
                    break;
            }
            return child;
        }



        private void DeterminateFavButtonVisibility()
        {
            try
            {
                var a = (from PoemItem p in App.favorites
                         where p.ID == listOfPoems[currentPoemIndex].ID
                         select p).ToList();
                if (a.Count() > 0)
                {
                    AppBarFavButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    AppBarUnFavButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
                else
                {
                    AppBarFavButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    AppBarUnFavButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while determinating relevant favorite button.", ex);
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            // TODO: Save the unique state of the page here.
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.None;
            GoogleAnalytics.EasyTracker.GetTracker().SendView("ItemPage");
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        /* private void AppBarPrevButton_Click(object sender, RoutedEventArgs e)
        {
            if (l.Count == 0) { return; }
            int j = 0;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].ID == db.id)
                {
                    j = i;
                    break;
                }
            }

            j -= 1;
            if (j < 0)
            {
                j = l.Count - 1;
            }
            DataBridge2 dba = new DataBridge2()
            {
                parentid = db.parentid,
                id = l[j].ID,
                title = l[j].Text
            };
            Frame.Navigate(typeof(ItemPage), Newtonsoft.Json.JsonConvert.SerializeObject(dba));
        }

        private void AppBarNextButton_Click(object sender, RoutedEventArgs e)
        {
            if (l.Count == 0) { return; }
            int j = l.Count - 1;
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].ID == db.id)
                {
                    j = i;
                    break;
                }
            }

            j += 1;
            if (j >= l.Count)
            {
                j = 0;
            }
            DataBridge2 dba = new DataBridge2()
            {
                parentid = db.parentid,
                id = l[j].ID,
                title = l[j].Text
            };
            Frame.Navigate(typeof(ItemPage), Newtonsoft.Json.JsonConvert.SerializeObject(dba));
        }
        */
        private void AppBarFavButton_Click(object sender, RoutedEventArgs e)
        {
            App.dataSource.AddToFavorites(listOfPoems[currentPoemIndex].ID);
            AppBarUnFavButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            AppBarFavButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "Like", ((PoemPivotItemData)ItemsPivot.SelectedItem).Header + "، " + App.ItemPageInitializeData.parentid, App.ItemPageInitializeData.id);
        }

        private void AppBarUnFavButton_Click(object sender, RoutedEventArgs e)
        {
            App.dataSource.RemoveFromFavorites(listOfPoems[currentPoemIndex].ID);
            AppBarUnFavButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            AppBarFavButton.Visibility = Windows.UI.Xaml.Visibility.Visible;

            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "Unlike", ((PoemPivotItemData)ItemsPivot.SelectedItem).Header + "، " + App.ItemPageInitializeData.parentid, App.ItemPageInitializeData.id);
        }

        private void AppBarShareButton_Click(object sender, RoutedEventArgs e)
        {
            currentVerse = "";
            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "Share", ((PoemPivotItemData)ItemsPivot.SelectedItem).Header + "، " + App.ItemPageInitializeData.parentid, App.ItemPageInitializeData.id);
            Windows.ApplicationModel.DataTransfer.DataTransferManager.ShowShareUI();
        }

        private void Verse_Holding(object sender, HoldingRoutedEventArgs e)
        {
            FrameworkElement senderElement = sender as FrameworkElement;
            FlyoutBase flyoutBase = FlyoutBase.GetAttachedFlyout(senderElement);

            flyoutBase.ShowAt(senderElement);
        }

        private void FlyoutShareButton_Click(object sender, RoutedEventArgs e)
        {
            var menuFlyoutItem = sender as MenuFlyoutItem;
            if (menuFlyoutItem != null)
            {
                var verse = menuFlyoutItem.DataContext as VerseItem;
                if (verse != null)
                {
                    currentVerse = verse.hemistich1;
                    if (verse.hemistich2Visibility == Windows.UI.Xaml.Visibility.Visible)
                    {
                        currentVerse += " / " + verse.hemistich2;
                    }
                    Windows.ApplicationModel.DataTransfer.DataTransferManager.ShowShareUI();
                }
            }
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(AboutPage));
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SettingsPage));
        }

        private async void ItemsPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int sIndex = ItemsPivot.SelectedIndex;

            try
            {
                if (((PoemPivotItemData)ItemsPivot.SelectedItem).Header.Length > 0)
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "Show", ((PoemPivotItemData)ItemsPivot.SelectedItem).Header + "، " + App.ItemPageInitializeData.parentid, 0);
            }
            catch { }

            if (InitialPivotLoading)
                return;

            /*currentPoemIndex += ItemsPivot.SelectedIndex - LoadedItemsBeforeCurrent;
            currentPoemIndex = FixRangeOfIndex(currentPoemIndex);*/

           /* if (listOfPoems.Count <= (LoadedItemsBeforeCurrent + LoadedItemsAfterCurrent + 1)) //No virtualization
            {
                int d = sIndex - currentPoemIndex;
                if (Math.Abs(d) == 1)
                    currentPoemIndex += Math.Sign(d);
                else
                    currentPoemIndex -= Math.Sign(d);
            }
            else
                if (sIndex > LoadedItemsBeforeCurrent) //Swiped to next page
                    currentPoemIndex += 1;
                else if (sIndex < LoadedItemsBeforeCurrent) //Swiped to previous page
                    currentPoemIndex -= 1;

            currentPoemIndex = FixRangeOfIndex(currentPoemIndex);
            */

            currentPoemIndex = ((PoemPivotItemData)ItemsPivot.SelectedItem).PoemIndex;

            if (!(listOfPoems.Count <= (LoadedItemsBeforeCurrent + LoadedItemsAfterCurrent + 1))) //virtualization
            {
                if ((ItemsPivot.SelectedIndex > (LoadedItemsBeforeCurrent + CompletelyLoadedItemsAfterCurrent)) || (ItemsPivot.SelectedIndex < (LoadedItemsBeforeCurrent - CompletelyLoadedItemsBeforeCurrent)))
                {
                    Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                    {
                        InitialPivotLoading = true;
                        await Task.Delay(100);
                        currentPoemIndex = ((PoemPivotItemData)ItemsPivot.SelectedItem).PoemIndex;
                        PivotPages.Clear();
                        NavigationHelper_LoadState(this, new LoadStateEventArgs("", null));
                    });
                    return;
                }
            }

            while (PivotLoading)
            {
                await Task.Delay(50);
            }

            Windows.Storage.ApplicationData.Current.LocalSettings.Values["ViewedItemID"] = currentPoemIndex;


            //App.ViewedItemID = currentPoemIndex;
            System.Diagnostics.Debug.WriteLine("ViewedItemID/currentPoemIndex is now equal to " + currentPoemIndex + " which means " + listOfPoems[currentPoemIndex].Text);
            DeterminateFavButtonVisibility();

            /*************************/
            //App.ItemPageInitializeData.id = listOfPoems[currentPoemIndex].ID;

            System.Diagnostics.Debug.WriteLine(ItemsPivot.SelectedIndex);
            //  SetScrollPosition();



            if (listOfPoems.Count <= (LoadedItemsBeforeCurrent + LoadedItemsAfterCurrent + 1)) //No virtualization
                return;

            if (sIndex > LoadedItemsBeforeCurrent) //Swiped to next page
            {
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                {
                    try
                    {
                        PivotLoading = true;
                        LoadPivotItem(currentPoemIndex + LoadedItemsAfterCurrent, true, false, false);
                        RemovePivotItemAfterDelay(false, 1000);
                        PivotLoading = false;
                    }
                    catch
                    {
                        System.Diagnostics.Debug.WriteLine("ERROR1");
                    }
                });
            }
            else if (sIndex < LoadedItemsBeforeCurrent) //Swiped to previous page
            {
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    try
                    {
                        PivotLoading = true;
                        LoadPivotItem(currentPoemIndex - LoadedItemsBeforeCurrent, false, false, false);
                        //RemovePivotItemAfterDelay(true, 600);
                        RemovePivotItem(true);
                        PivotLoading = false;
                    }
                    catch
                    {
                        System.Diagnostics.Debug.WriteLine("ERROR2");
                    }
                });
            }

            //fix the current page
            for (int i = -CompletelyLoadedItemsBeforeCurrent; i <= CompletelyLoadedItemsAfterCurrent; i++)
            {
                LoadVersesToPage(i);
            }
        }

        private async void RemovePivotItemAfterDelay(bool last, int milliseconds)
        {
            try
            {
                await Task.Delay(milliseconds);
                RemovePivotItem(last);
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("WillWait");
                RemovePivotItemAfterDelay(last, 100); //If list were busy right now, we'll remove item later.
            }
        }

        private double GetScrollPosition(int poemId)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            object scrollPos = localSettings.Values["scrollPos" + poemId.ToString()];
            if (scrollPos != null)
            {
                try
                {
                    return double.Parse(scrollPos.ToString());
                }
                catch
                {
                    return 0.0;
                }
            }
            else
            {
                return 0.0;
            }
        }

        /* private void SetScrollPosition()
        {
            var curPage = ItemsPivot.ItemContainerGenerator.ContainerFromIndex(ItemsPivot.SelectedIndex);
            Task.Run(async () =>
            {
                await Task.Delay(100);
                ScrollViewer sv = null;
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
                {
                    sv = GetVisualChild<ScrollViewer>(curPage);
                });
                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                object scrollPos = localSettings.Values["scrollPos" + App.ItemPageInitializeData.id];
                if (scrollPos != null)
                {
                    try
                    {
                        await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
                        {
                            sv.ChangeView(0, double.Parse(scrollPos.ToString()), 1, false);
                        });
                    }
                    catch
                    {
                        localSettings.Values["scrollPos" + App.ItemPageInitializeData.id] = 0;
                    }
                }
            });
        }
        */
        private async void LoadVersesToPage(int offset)
        {
            int idx = ItemsPivot.SelectedIndex + offset;
            if (idx < 0)
                idx = ItemsPivot.Items.Count - 1;
            else if (idx > (ItemsPivot.Items.Count - 1))
                idx = 0;

            if ((PivotPages[idx].PoemItems.Count > 0) && (PivotPages[idx].Header.Replace("\t", "").Length > 0)) //Already Loaded
                return;

            int index = PivotPages[idx].PoemIndex;//FixRangeOfIndex(currentPoemIndex + offset);


            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
            {
                PivotLoading = true;
                List<VerseItem> v = new List<VerseItem>();
                v = ReadVersesWithProperFontData(index, v);

                PoemPivotItemData data = new PoemPivotItemData()
                {
                    Header = listOfPoems[index].Text,
                    PoemItems = v,
                    PoemIndex = index,
                    ScrollPosition = GetScrollPosition(listOfPoems[index].ID)
                };

                PivotPages[idx] = data;
                PivotLoading = false;
                System.Diagnostics.Debug.WriteLine("Loaded verses of " + data.Header);
            });

        }

        private void RemovePivotItem(bool last)
        {

            System.Diagnostics.Debug.WriteLine("Removed " + PivotPages[last ? (PivotPages.Count - 1) : 0].Header + " from the list.");
            PivotPages.RemoveAt(last ? (PivotPages.Count - 1) : 0);
        }


        /// <summary>
        /// Adds a new poem page to the Pivot items.
        /// </summary>
        /// <param name="index">Index of poem</param>
        /// <param name="AtTheEnd">If true, new page will be added to the end of the items. If false, new page will be added to the beginning of the items.</param>
        private void LoadPivotItem(int index, bool AtTheEnd, bool readVersesCompletely, bool insertTab)
        {
            //If index is out of range, correct it.
            index = FixRangeOfIndex(index);

            //Don't add an item twice
            if (PivotPages.Where(x => x.PoemIndex == index).Count() > 0)
                return;

            //Retrieve data and add page to collection.
            List<VerseItem> v = new List<VerseItem>();

            if (readVersesCompletely)
            {
                v = ReadVersesWithProperFontData(index, v);
            }

            PoemPivotItemData data = new PoemPivotItemData()
            {
                PoemItems = v,
                PoemIndex = index,
                ScrollPosition = GetScrollPosition(listOfPoems[index].ID)
            };

            if (!insertTab)
                data.Header = listOfPoems[index].Text;
            else
                data.Header = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";

            try
            {
                if (AtTheEnd)
                    PivotPages.Add(data);
                else
                    PivotPages.Insert(0, data);
            }
            catch (Exception ex)
            {
                index = index;
            }
            System.Diagnostics.Debug.WriteLine("Added " + listOfPoems[index].Text + " to the list.");
        }

        private int FixRangeOfIndex(int index)
        {
            while (index < 0)
                index += listOfPoems.Count;

            while (index >= listOfPoems.Count)
                index -= listOfPoems.Count;
            return index;
        }

        private List<VerseItem> ReadVersesWithProperFontData(int index, List<VerseItem> v)
        {
            v = App.dataSource.GetVerses(listOfPoems[index].ID);

            try
            {
                Brush w;
                if (App.Current.RequestedTheme == ApplicationTheme.Dark)
                {
                    w = new SolidColorBrush(Windows.UI.Colors.White);
                }
                else
                {
                    w = new SolidColorBrush(Windows.UI.Colors.Black);
                }
                for (int i = 0; i < v.Count; i++)
                {
                    v[i].ForegroundColor1 = w;
                    v[i].ForegroundColor2 = w;
                }

                double fSize;
                int lHeight = 0; //Automatic
                string sF = "";
                bool needsYeCorrection = false;
                if (App.DefaultFont != null)
                {
                    sF = App.DefaultFont.Source.ToString();
                }
                if (sF.Contains("Nazanin"))
                {
                    fSize = 24;
                    needsYeCorrection = true;
                }
                else if (sF.Contains("Kamran"))
                {
                    fSize = 26;
                    needsYeCorrection = true;
                }
                else if (sF.Contains("Nastaliq"))
                {
                    fSize = 44;
                }
                else
                {
                    fSize = 20;
                    lHeight = 30;
                }

                for (int i = 0; i < v.Count; i++)
                {
                    if (needsYeCorrection)
                    {
                        v[i].hemistich1 = v[i].hemistich1.Replace("هٔ", "ة‌"); //Haz nim-faseleh after ة
                        v[i].hemistich2 = v[i].hemistich2.Replace("هٔ", "ة‌"); //Haz nim-faseleh after ة
                    }
                    if (App.DefaultFont != null)
                    {
                        v[i].font = App.DefaultFont;
                    }
                    v[i].fontSize = fSize;
                    v[i].lineHeight = lHeight;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while setting font data.", ex);
            }
            try
            {
                if (App.SearchQuery.Length > 0)
                {
                    Brush y;
                    if (App.Current.RequestedTheme == ApplicationTheme.Dark)
                    {
                        y = new SolidColorBrush(Windows.UI.Colors.Yellow);
                    }
                    else
                    {
                        y = new SolidColorBrush(Windows.UI.Colors.DarkRed);
                    }
                    for (int i = 0; i < v.Count; i++)
                    {
                        if (v[i].hemistich1.Contains(App.SearchQuery))
                        {
                            v[i].ForegroundColor1 = y;
                            if (firstSearchItem == 0)
                                firstSearchItem = i;
                        }

                        if (v[i].hemistich2.Contains(App.SearchQuery))
                        {
                            v[i].ForegroundColor2 = y;
                            if (firstSearchItem == 0)
                                firstSearchItem = i;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("Error while setting text color.", ex);
            }
            return v;
        }

        private async void itemListView_Loaded(object sender, RoutedEventArgs e)
        {
            await Task.Delay(100);

            if (firstSearchItem != 0) //Search mode
            {
                ListView sdr = ((ListView)sender);
                if ((firstSearchItem + 2) < (sdr.Items.Count - 1))
                    sdr.ScrollIntoView(sdr.Items[firstSearchItem + 2]);
                else
                {
                    ScrollViewer sv = GetVisualChild<ScrollViewer>((ListView)sender);
                    if (sv != null)
                    {
                        sv.ChangeView(0, sv.ScrollableHeight + 1000.0, 1, false);
                    }
                }
                return;
            }

            double scrollPos = 0.0;
            if (double.TryParse(((ListView)sender).Tag.ToString(), out scrollPos))
            {
                ScrollViewer sv = GetVisualChild<ScrollViewer>((ListView)sender);
                if (sv != null)
                {
                    sv.ChangeView(0, scrollPos, 1, false);
                    if (scrollPos > 50)
                    {
                        var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                        /**
                        if (localSettings.Values["NewGanjineCanRememberPosition"] == null)
                        {
                            await Task.Delay(1000);
                            GanjinehCanRememberPositionNow.Visibility = Windows.UI.Xaml.Visibility.Visible;
                            Storyboard sb = this.Resources["WhatsNewPanelShow"] as Storyboard;
                            sb.Begin();
                            await Task.Delay(7000);
                            Storyboard sb2 = this.Resources["WhatsNewPanelHide"] as Storyboard;
                            sb2.Begin();
                            await Task.Delay(200);
                            GanjinehCanRememberPositionNow.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                            localSettings.Values["NewGanjineCanRememberPosition"] = "1";
                        }
                        /**/
                    }
                }
            }
        }

        /**
        private async void GanjinehCanRememberPositionNow_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Storyboard sb2 = this.Resources["WhatsNewPanelHide"] as Storyboard;
            sb2.Begin();
            await Task.Delay(200);
            GanjinehCanRememberPositionNow.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }
        /**/

        private void AppBarSelectButton_Click(object sender, RoutedEventArgs e)
        {
            QKit.MultiSelectListView lv = GetVisualChild<QKit.MultiSelectListView>(ItemsPivot.ItemContainerGenerator.ContainerFromIndex(ItemsPivot.SelectedIndex));
            lv.SelectionMode = ListViewSelectionMode.Multiple;

        }

        private void itemListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            QKit.MultiSelectListView lv = GetVisualChild<QKit.MultiSelectListView>(ItemsPivot.ItemContainerGenerator.ContainerFromIndex(ItemsPivot.SelectedIndex));
            if (lv == null)
                return;
            if (lv.SelectionMode == ListViewSelectionMode.None)
                return;

            if (lv.SelectedItems.Contains(e.ClickedItem))
                lv.SelectedItems.Remove(e.ClickedItem);
            else
                lv.SelectedItems.Add(e.ClickedItem);
        }

        private void AppBarFinishSelectButton_Click(object sender, RoutedEventArgs e)
        {
            QKit.MultiSelectListView lv = GetVisualChild<QKit.MultiSelectListView>(ItemsPivot.ItemContainerGenerator.ContainerFromIndex(ItemsPivot.SelectedIndex));
            lv.SelectionMode = ListViewSelectionMode.None;
        }

        private void itemListView_SelectionModeChanged(object sender, RoutedEventArgs e)
        {
            QKit.MultiSelectListView lv = (QKit.MultiSelectListView)sender;
            if (lv.SelectionMode == ListViewSelectionMode.None)
            {
                DeterminateFavButtonVisibility();
                AppBarSelectButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                AppBarFinishSelectButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                ItemsPivot.IsLocked = false;
            }
            else
            {
                AppBarFavButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                AppBarUnFavButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                AppBarSelectButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                AppBarFinishSelectButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                ItemsPivot.IsLocked = true;
            }
        }

        private void TextBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            /*await Task.Delay(250);
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Low, () =>
            {
                int poemIdx;
                if (int.TryParse(((TextBlock)sender).Tag.ToString(), out poemIdx))
                {
                    if (poemIdx != currentPoemIndex)
                    {
                        int nextVal = ItemsPivot.SelectedIndex + 1;
                        if (nextVal > (ItemsPivot.Items.Count - 1))
                        {
                            nextVal = 0;
                        }
                        ItemsPivot.SelectedIndex = nextVal;
                        ItemsPivot.UpdateLayout();

                    }
                }
            });*/
            int poemIdx;
            if ((!InitialPivotLoading) && (int.TryParse(((TextBlock)sender).Tag.ToString(), out poemIdx)))
            {
                if (poemIdx != currentPoemIndex)
                {
                    int nextVal = ItemsPivot.SelectedIndex + 1;
                    if (nextVal > (ItemsPivot.Items.Count - 1))
                    {
                        nextVal = 0;
                    }
                    ItemsPivot.SelectedIndex = nextVal;
                    ItemsPivot.UpdateLayout();
                    /*
                    GanjineSwipeLeftRight.Opacity = 0;
                    GanjineSwipeLeftRight.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    Storyboard sb = this.Resources["SwipePanelShow"] as Storyboard;
                    sb.Begin();*/
                }
            }
            e.Handled = true;
        }

        private async void GanjineSwipeLeftRight_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            Storyboard sb = this.Resources["SwipePanelHide"] as Storyboard;
            sb.Begin();
            await Task.Delay(200);
            GanjineSwipeLeftRight.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Windows.UI.ViewManagement.ApplicationView.GetForCurrentView().Orientation == Windows.UI.ViewManagement.ApplicationViewOrientation.Portrait)
            {
                ItemsPivot.Margin = new Thickness(0, 0, 0, 0);
            }
            else
            {
                ItemsPivot.Margin = new Thickness(0, 0, 0, 0);
            }
        }



    }
}