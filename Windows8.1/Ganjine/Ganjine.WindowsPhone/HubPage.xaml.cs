﻿using Ganjine.Common;
using Ganjine.DataModel;
using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Background;
using Windows.ApplicationModel.DataTransfer;
using Windows.ApplicationModel.Email;
using Windows.ApplicationModel.Resources;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Universal Hub Application project template is documented at http://go.microsoft.com/fwlink/?LinkID=391955

namespace Ganjine
{


    /// <summary>
    /// A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class HubPage : Page
    {
        private readonly NavigationHelper navigationHelper;
        private readonly ObservableDictionary defaultViewModel = new ObservableDictionary();
        private readonly ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView("Resources");
        private readonly string defaultPoetOrder = "{\"2\":0,\"3\":1,\"4\":2,\"5\":3,\"6\":4,\"7\":5,\"8\":6,\"9\":7,\"10\":8,\"11\":9,\"12\":10,\"13\":11,\"14\":12,\"15\":13,\"27\":14,\"28\":15,\"29\":16,\"16\":17,\"35\":18,\"17\":19,\"501\":20,\"502\":21,\"503\":22,\"504\":23,\"506\":24,\"612\":25,\"701\":26,\"18\":27,\"19\":28,\"20\":29,\"21\":30,\"22\":31,\"23\":32,\"24\":33,\"25\":34,\"26\":35,\"30\":36,\"31\":37,\"32\":38,\"33\":39,\"34\":40,\"37\":41,\"38\":42,\"39\":43,\"40\":44,\"41\":45,\"42\":46,\"43\":47,\"44\":48,\"45\":49,\"46\":50,\"47\":51,\"48\":52,\"49\":53,\"50\":54,\"51\":55,\"52\":56,\"53\":57,\"54\":58,\"55\":59,\"56\":60}";

        private TextBox searchBox;
        private ComboBox searchSource;
        private CheckBox searchExact;
        private ListView searchListView;
        private ListView poetsListView;
        private ProgressBar searchProgressBar;
        bool CancelSearch;
        string sErrorLog;

        public HubPage()
        {
            this.InitializeComponent();

            // Hub is only supported in Portrait orientation
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;

            this.NavigationCacheMode = NavigationCacheMode.Required;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;


            HardwareButtons.BackPressed += HardwareButtons_BackPressed;


            string themeFileName;
            int theme = (int)Windows.Storage.ApplicationData.Current.LocalSettings.Values["currentTheme"];
            if (theme == 1)
            {
                themeFileName = "dark";
            }
            else if (theme == 2)
            {
                themeFileName = "orange";
            }
            else if (theme == 3)
            {
                themeFileName = "blue";
            }
            else //0
            {
                themeFileName = "light";
            }

         /**/   if (App.Current.RequestedTheme == ApplicationTheme.Dark)
            {
                LayoutRoot.Background = new SolidColorBrush(Windows.UI.Colors.Black);
                Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ForegroundColor = Windows.UI.Colors.LightGray;
            }
            else
            {
                LayoutRoot.Background = new SolidColorBrush(Windows.UI.Colors.White);
                Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ForegroundColor = Windows.UI.Colors.DarkGray;
            }
            /**/

            ImageSource backgroundImage = new BitmapImage(new Uri(Package.Current.InstalledLocation.Path + "\\Assets\\HubBackground.theme-" + themeFileName + ".png"));
            ImageBrush ib = new ImageBrush();
            ib.ImageSource = backgroundImage;
            Hub.Background = ib;


        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            try
            {
                if (poetsListView.SelectionMode == ListViewSelectionMode.Multiple)
                {
                    FinishEditPoetsButton_Click(this, new RoutedEventArgs());
                    e.Handled = true;
                }
            }
            catch { }
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            App.ItemPageInitializeData = null;
            App.CurPageArg = "";

            Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                await Task.Delay(100);
                if (App.ViewedSectionID.ContainsKey("main"))
                {
                    poetsListView.UpdateLayout();
                    int selectedItemID = App.ViewedSectionID["main"];
                    App.ViewedSectionID.Remove("main");
                    int index = 0;
                    for (int i = 0; i < poetsListView.Items.Count; i++)
                    {
                        if (((PoetItem)poetsListView.Items[i]).PoetID == selectedItemID)
                        {
                            index = i;
                        }
                    }
                    index += 4;
                    if (index > (poetsListView.Items.Count - 1))
                    {
                        index = poetsListView.Items.Count - 1;
                    }
                    poetsListView.ScrollIntoView(poetsListView.Items[index]);
                }
            });



            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            if (localSettings.Values["wideTileUpdate"] == null)
            {
                localSettings.Values["wideTileUpdate"] = true;
            }

            if ((bool)localSettings.Values["wideTileUpdate"] == true)
            {
                //Update background task live tile updater
                string appVersion = string.Format("{0}.{1}.{2}.{3}",
                Package.Current.Id.Version.Build,
                Package.Current.Id.Version.Major,
                Package.Current.Id.Version.Minor,
                Package.Current.Id.Version.Revision);

                if ((Windows.Storage.ApplicationData.Current.LocalSettings.Values["AppVersion"] != null) && (Windows.Storage.ApplicationData.Current.LocalSettings.Values["AppVersion"].ToString() != appVersion))
                {
                    // Our app has been updated
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values["AppVersion"] = appVersion;

                    // Call RemoveAccess
                    BackgroundExecutionManager.RemoveAccess();
                }

                BackgroundAccessStatus status = await BackgroundExecutionManager.RequestAccessAsync();

                var taskRegistered = false;
                var taskName = "GanjineBackgroundTask";

                foreach (var task in BackgroundTaskRegistration.AllTasks)
                {
                    if (task.Value.Name == taskName)
                    {
                        taskRegistered = true;
                        break;
                    }
                }

                if (!taskRegistered)
                {
                    var builder = new BackgroundTaskBuilder();

                    builder.Name = taskName;
                    builder.TaskEntryPoint = "GanjineBackgroundTaskProj.GanjineBackgroundTask";
                    builder.SetTrigger(new TimeTrigger(45, false));
                    builder.AddCondition(new SystemCondition(SystemConditionType.UserPresent));
                    BackgroundTaskRegistration task = builder.Register();
                }
            }
            else
            {
                foreach (var item in BackgroundTaskRegistration.AllTasks.Values)
                {
                    item.Unregister(false);
                }
            }




            Windows.Storage.ApplicationData.Current.LocalSettings.Values["ViewedItemID"] = -1;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            // TODO: Save the unique state of the page here.
        }

        /// <summary>
        /// Shows the details of a clicked group in the <see cref="SectionPage"/>.
        /// </summary>
        /// <param name="sender">The source of the click event.</param>
        /// <param name="e">Details about the click event.</param>
        private void GroupSection_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        /// <summary>
        /// Shows the details of an item clicked on in the <see cref="ItemPage"/>
        /// </summary>
        /// <param name="sender">The source of the click event.</param>
        /// <param name="e">Defaults about the click event.</param>
        private void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (poetsListView.SelectionMode == ListViewSelectionMode.Multiple)
            {
                if (poetsListView.SelectedItems.Contains(e.ClickedItem))
                {
                    poetsListView.SelectedItems.Remove(e.ClickedItem);
                }
                else
                {
                    poetsListView.SelectedItems.Add(e.ClickedItem);
                }
                return;
            }
            if (Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ProgressIndicator.Text.Length > 0)
            {
                CancelSearch = true;
                Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ProgressIndicator.Text = "";
                Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ProgressIndicator.ProgressValue = null;
                Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ProgressIndicator.HideAsync();
            }
            Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
            {
                var poet = (PoetItem)(e.ClickedItem);
                DataBridge1 db = new DataBridge1()
                {
                    id = poet.CatID,
                    title = poet.PoetName,
                    poetid = poet.PoetID,
                    rootPath = ""
                };

                if (App.ViewedSectionID.ContainsKey("main"))
                {
                    App.ViewedSectionID.Remove("main");
                }
                App.ViewedSectionID.Add("main", poet.PoetID);
                Frame.Navigate(typeof(SectionPage), Newtonsoft.Json.JsonConvert.SerializeObject(db));
            });

        }


        private async Task CopyBigFile(StorageFile fileSource, StorageFile fileDest, CancellationToken ct)
        {
            using (Stream streamSource = await fileSource.OpenStreamForReadAsync())
            using (Stream streamDest = await fileDest.OpenStreamForWriteAsync())
                await streamSource.CopyToAsync(streamDest, 1024, ct);
            return;
        }

        private async Task CopyDatabaseAsync()
        {
            if (App.conn != null)
            {
                return;
            }

            bool versionMismatch = false;
            bool isDatabaseExisting = false;
            StorageFile storageFile = null;
            StorageFile databaseFile;

            try
            {
                databaseFile = await Package.Current.InstalledLocation.GetFileAsync("ganjoor2.s3db");
            }
            catch (Exception ex)
            {
                throw new Exception("Couldn't get db file.", ex);
            }

            try
            {
                StorageFile versionFile = await ApplicationData.Current.LocalFolder.GetFileAsync("version.txt");
                StorageFile currentVersionFile = await Package.Current.InstalledLocation.GetFileAsync("version.txt");
                int version = int.Parse(await Windows.Storage.FileIO.ReadTextAsync(versionFile));
                int currentVersion = int.Parse(await Windows.Storage.FileIO.ReadTextAsync(currentVersionFile));
                if (version != currentVersion)
                {
                    versionMismatch = true;
                }
            }
            catch (Exception ex)
            {
                LoadingGrid.Visibility = Windows.UI.Xaml.Visibility.Visible;
                commandBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Hub.IsEnabled = false;

                versionMismatch = true;
            }

            try
            {
                storageFile = await ApplicationData.Current.LocalCacheFolder.GetFileAsync("ganjoor2.s3db");
                if (versionMismatch)
                {
                    try
                    {
                        App.conn.Dispose();
                        await Task.Delay(TimeSpan.FromSeconds(0.5));
                    }
                    catch { }
                    await storageFile.DeleteAsync();
                }
                else
                {
                    isDatabaseExisting = true;
                    if ((await storageFile.GetBasicPropertiesAsync()).Size != (await databaseFile.GetBasicPropertiesAsync()).Size) //Size mismatch
                    {
                        versionMismatch = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoadingGrid.Visibility = Windows.UI.Xaml.Visibility.Visible;
                commandBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Hub.IsEnabled = false;
                isDatabaseExisting = false;
            }

            if (versionMismatch)
            {
                StorageFile currentVersionFile = await Package.Current.InstalledLocation.GetFileAsync("version.txt");

                //Delete old version file, if exists.
                try
                {
                    StorageFile vFile = await ApplicationData.Current.LocalFolder.GetFileAsync("version.txt");
                    await vFile.DeleteAsync();
                }
                catch
                {

                }

                await currentVersionFile.CopyAsync(ApplicationData.Current.LocalFolder);
                /*if (isDatabaseExisting == true)
                {
                    LoadingGrid.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    commandBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    Hub.IsEnabled = false;
                    bool CriticalError = false;
                    
                    try
                    {
                        storageFile = null;
                        storageFile = await ApplicationData.Current.LocalCacheFolder.GetFileAsync("ganjoor2.s3db");
                        await storageFile.DeleteAsync();
                    }
                    catch (Exception ex)
                    {
                        CriticalError = true;
                    }
                    if (CriticalError)
                    {
                        Hub.Visibility = Visibility.Collapsed;
                        LoadingGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        MessageDialog md = new MessageDialog("مشکلی در راه‌اندازی اولیه‌ی برنامه پیش آمده است. با عرض پوزش، لطفاً برنامه را از گوشی خود پاک کرده و از نو نصب کنید.");
                        await md.ShowAsync();
                        App.Current.Exit();
                        return;
                    }
                    isDatabaseExisting = false;
                }*/
            }

            if (!isDatabaseExisting)
            {


                try
                {
                    //StorageFile newfile = await ApplicationData.Current.LocalCacheFolder.CreateFileAsync("ganjoor2.s3db");
                    //await CopyBigFile(databaseFile, newfile, CancellationToken.None);
                    await databaseFile.CopyAsync(ApplicationData.Current.LocalCacheFolder, "ganjoor2.s3db", NameCollisionOption.ReplaceExisting);
                }
                catch (Exception ex)
                {
                    throw new Exception("Couldn't copy database file to local cache folder.", ex);
                }
            }
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;
            //Copy database to local storage (after first installation, or database update)


            bool err = false;


            this.navigationHelper.OnNavigatedTo(e);

            await CopyDatabaseAsync();

            //Create connection with database
            App.dataSource.CheckDBConnection();

            try
            {
                if (!this.defaultViewModel.ContainsKey("Poets"))
                {
                    //Read list of poets.
                    Dictionary<int, int> orders;
                    ObservableCollection<PoetItem> p1 = App.dataSource.GetListOfPoets();

                    var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

                    if (localSettings.Values["poetOrders"] == null) //Default order
                    {
                        localSettings.Values["poetOrders"] = defaultPoetOrder;
                    }
                    orders = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<int, int>>(localSettings.Values["poetOrders"].ToString());

                    foreach (var item in p1)
                    {
                        item.OrderID = orders[item.PoetID];
                    }

                    p1 = new ObservableCollection<PoetItem>(p1.OrderBy(a => a.OrderID));

                    this.defaultViewModel["Poets"] = p1;





                    ObservableCollection<PoetItem> pp = new ObservableCollection<PoetItem>();
                    pp.Add(new PoetItem() { PoetID = -10, PoetName = "" });
                    pp.Add(new PoetItem() { PoetID = -1, PoetName = "همه‌ی آثار" });
                    foreach (var item in p1)
                    {
                        pp.Add(item);
                    }

                    this.defaultViewModel["Sources"] = pp;

                    try { searchSource.SelectedIndex = 0; }
                    catch { }
                }
                ShowFavorites();

                Hub.IsEnabled = true;
                LoadingGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                commandBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            catch (Exception ex)
            {
                if (ex.ToString().ToLower().Contains("out of memory"))
                {
                    err = true;
                }
                else
                {
                    throw ex;
                }
            }
            if (err)
            {
                MessageDialog mmd = new MessageDialog("برای ادامه‌ی کار، برنامه را مجدداً اجرا کنید.");
                await mmd.ShowAsync();
                await Task.Delay(TimeSpan.FromSeconds(0.5));
                App.Current.Exit();
            }
        }

        private void ShowFavorites()
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            if (localSettings.Values["favOrderBy"] == null)
            {
                localSettings.Values["favOrderBy"] = 2;
            }

            List<PoemItem> ppa = new List<PoemItem>();
            ppa.AddRange(App.favorites);
            for (int i = 0; i < ppa.Count; i++)
                if ((int)localSettings.Values["favOrderBy"] == 1)
                    ppa[i].OrderID = i;
                else
                    ppa[i].OrderID = ppa[i].ID;

            var groupedFavorites = QKit.JumpList.JumpListHelper.ToGroups(ppa, x => x.OrderID, x => x.PoetName);

            this.defaultViewModel["Favorites"] = groupedFavorites;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {

            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            //Windows.UI.ViewManagement.StatusBar.GetForCurrentView().BackgroundColor = Windows.UI.Color.FromArgb(255,3,255,3);

            //In order to constantly updating listView, we have to add Items manually, so we don't use ViewModel :)

            searchProgressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
            searchExact.IsEnabled = false;

            CancelSearch = false;
            string s = searchBox.Text;
            int i = ((PoetItem)searchSource.SelectedItem).PoetID;
            App.SearchSourceIndex = searchSource.SelectedIndex;
            App.SearchQuery = s;
            HubSection2.Width = Window.Current.Bounds.Width - 40;
            searchListView.Items.Clear();

            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Search", ((PoetItem)searchSource.SelectedItem).PoetName, s, 0);

            if (i > 0)
            {
                SearchButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;



                await SearchInsideAPoet(s, i, 500, searchExact.IsChecked == true);

                SearchButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else
            {
                SearchButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                CancelSearchButton.Visibility = Windows.UI.Xaml.Visibility.Visible;

                await Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ProgressIndicator.ShowAsync();
                Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ProgressIndicator.ProgressValue = 0;
                for (int x = 2; x < searchSource.Items.Count; x++)
                {

                    PoetItem pi = ((PoetItem)searchSource.Items[x]);
                    Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ProgressIndicator.Text = "در حال جست‌و‌جو در آثار " + pi.PoetName;
                    await SearchInsideAPoet(s, pi.PoetID, 100, searchExact.IsChecked == true);
                    Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ProgressIndicator.ProgressValue = ((double)x) / ((double)searchSource.Items.Count);
                    if (CancelSearch)
                    {
                        break;
                    }
                }
                Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ProgressIndicator.Text = "";
                Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ProgressIndicator.ProgressValue = null;
                await Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ProgressIndicator.HideAsync();

                SearchButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                CancelSearchButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }

            searchProgressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            searchExact.IsEnabled = true;

            if (searchListView.Items.Count == 0)
            {
                var dialog1 = new MessageDialog("موردی یافت نشد.");
                await dialog1.ShowAsync();
            }
        }

        private async Task SearchInsideAPoet(string s, int i, int l, bool exact)
        {
            DateTime beginSearch = DateTime.Now;

            List<PoemItem> pi = new List<PoemItem>();
            //Stopwatch ss = new Stopwatch();

            await Windows.System.Threading.ThreadPool.RunAsync(_ =>
            {
                //ss.Start();    
                //pi.AddRange((List<PoemItem>)this.defaultViewModel["SearchResults"]);
                pi.AddRange(App.dataSource.Search(s, i, l, exact ? AdvancedSearchMode.ExactPhrase : AdvancedSearchMode.Phrase));


                //ss.Stop();
            }, Windows.System.Threading.WorkItemPriority.High, Windows.System.Threading.WorkItemOptions.TimeSliced);
            /*var dialog = new MessageDialog("with await: " + ss.ElapsedMilliseconds.ToString() + "ms");
            await dialog.ShowAsync();*/

            foreach (PoemItem item in pi)
            {
                searchListView.Items.Add(item);
            }

            GoogleAnalytics.EasyTracker.GetTracker().SendTiming(DateTime.Now.Subtract(beginSearch), "Search", App.dataSource.GetPoetName(i), s);
        }

        private void SearchBox_Loaded(object sender, RoutedEventArgs e)
        {
            searchBox = (TextBox)sender;
        }

        private void SearchSource_Loaded(object sender, RoutedEventArgs e)
        {
            searchSource = (ComboBox)sender;

            if (searchSource.Items.Count == 0)
                return;

            searchSource.SelectedIndex = 0;
        }

        private void SearchSource_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //something wierd happened when selecting first item. so I'll did this :)
            if (searchSource.SelectedIndex == 0)
            {
                searchSource.SelectedIndex = 1;
            }
        }


        private void SearchListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            PoemItem pi = (PoemItem)e.ClickedItem;
            DataBridge2 db = new DataBridge2()
            {
                parentid = 0,
                id = pi.ID,
            };
            Frame.Navigate(typeof(ItemPage), Newtonsoft.Json.JsonConvert.SerializeObject(db));
        }



        private void FavItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            PoemItem pi = (PoemItem)e.ClickedItem;
            DataBridge2 db = new DataBridge2()
            {
                parentid = 0,
                id = pi.ID,
            };
            Frame.Navigate(typeof(ItemPage), Newtonsoft.Json.JsonConvert.SerializeObject(db));
        }

        private void ListView_Loaded(object sender, RoutedEventArgs e)
        {
            searchListView = (ListView)sender;

        }

        private void CancelSearchButton_Click(object sender, RoutedEventArgs e)
        {
            CancelSearchButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            CancelSearch = true;
        }

        private void Hub_SectionsInViewChanged(object sender, SectionsInViewChangedEventArgs e)
        {
            MoveUpButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            MoveDownButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            ResetOrderButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            FinishEditPoetsButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            if (Hub.SectionsInView[0].Name == "HubSection2")
            {
                EditPoetsButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                FavSortByButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                App.SearchQuery = searchBox.Text;
                if (Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ProgressIndicator.Text.Length > 0)
                {
                    CancelSearchButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
                else
                {
                    SearchButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
            }
            else if (Hub.SectionsInView[0].Name == "HubSection1")
            {
                App.SearchQuery = "";
                SearchButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                CancelSearchButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                EditPoetsButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                FavSortByButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                if (poetsListView != null) //Don't do this at startup!
                {
                    if (poetsListView.SelectionMode == ListViewSelectionMode.Multiple)
                    {
                        FinishEditPoetsButton_Click(null, new RoutedEventArgs());
                    }
                }
            }
            else
            {
                App.SearchQuery = "";
                SearchButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                CancelSearchButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                EditPoetsButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                FavSortByButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }


        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (searchBox.Text.Length > 1)
            {
                SearchButton.IsEnabled = true;
            }
            else
            {
                SearchButton.IsEnabled = false;
            }
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(AboutPage));
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SettingsPage));
        }

        private void ProgressBar_Loaded(object sender, RoutedEventArgs e)
        {
            searchProgressBar = (ProgressBar)sender;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //Force refresh poets list (BUGFIX of first poet not showing)
            try
            {
                ObservableCollection<PoetItem> pi = (ObservableCollection<PoetItem>)this.defaultViewModel["Poets"];
                pi.Move(0, 0);
            }
            catch { }



            //Update live tile with new 5 random poets
            try
            {
                if ((bool)Windows.Storage.ApplicationData.Current.LocalSettings.Values["wideTileUpdate"] == true)
                {
                    GanjineBackgroundTaskProj.GanjineBackgroundTask.UpdateMainTile();
                }
                else
                {
                    TileUpdateManager.CreateTileUpdaterForApplication().Clear();
                }
            }
            catch { }


            if (App.askForReview)
            {
                App.askForReview = false;
                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                localSettings.Values["AskedForReview"] = "YES";
                var messageDialog = new MessageDialog("آیا مایلید در استور به «گنجینه» امتیاز دهید؟");

                // Add commands and set their callbacks; both buttons use the same callback function instead of inline event handlers
                messageDialog.Commands.Add(new UICommand(
                    "بله",
                    new UICommandInvokedHandler(this.RateApp)));
                messageDialog.Commands.Add(new UICommand(
                    "خیر",
                    new UICommandInvokedHandler(this.DontRateApp)));

                // Set the command that will be invoked by default
                messageDialog.DefaultCommandIndex = 0;

                // Set the command to be invoked when escape is pressed
                messageDialog.CancelCommandIndex = 1;

                // Show the message dialog
                await messageDialog.ShowAsync();
            }

            //Search again if app resumed and results are gone.
            if (searchListView != null)
            {
                if (searchListView.Items.Count == 0)
                {
                    if (App.SearchQuery.Length > 0)
                    {
                        searchBox.Text = App.SearchQuery;
                        searchSource.SelectedIndex = App.SearchSourceIndex;
                        SearchButton_Click(SearchButton, new RoutedEventArgs());
                    }
                }
            }

            GoogleAnalytics.EasyTracker.GetTracker().SendView("MainPage");
        }


        private async void RateApp(IUICommand command)
        {
            await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-windows-store:reviewapp?appid=f7ba92a3-ca3c-4252-84ab-82eb3e4b0c62"));
        }

        private void DontRateApp(IUICommand command)
        {
            //Do nothing!
        }

        private void PoetsListView_Loaded(object sender, RoutedEventArgs e)
        {
            poetsListView = (ListView)sender;
        }

        private async void EditPoetsButton_Click(object sender, RoutedEventArgs e)
        {

            ObservableCollection<PoetItem> pi = (ObservableCollection<PoetItem>)this.defaultViewModel["Poets"];
            foreach (PoetItem item in pi)
            {
                item.ImageVisibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
            pi.Move(0, 0);

            poetsListView.SelectionMode = ListViewSelectionMode.Multiple;
            EditPoetsButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            MoveUpButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            MoveDownButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            ResetOrderButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            FinishEditPoetsButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void MoveUpButton_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<PoetItem> pi = (ObservableCollection<PoetItem>)this.defaultViewModel["Poets"];
            List<PoetItem> selItems = (from PoetItem p in pi
                                       where poetsListView.SelectedItems.Contains(p)
                                       select p).ToList();
            foreach (PoetItem item in selItems)
            {
                int i = pi.IndexOf(item);
                if (i > 0)
                {
                    pi.RemoveAt(i);
                    pi.Insert(i - 1, item);
                }
            }
            foreach (PoetItem item in selItems)
            {
                poetsListView.SelectedItems.Add(item);
            }
            poetsListView.ScrollIntoView(selItems[0]);
        }

        private void MoveDownButton_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<PoetItem> pi = (ObservableCollection<PoetItem>)this.defaultViewModel["Poets"];
            List<PoetItem> selItems = (from PoetItem p in pi
                                       where poetsListView.SelectedItems.Contains(p)
                                       select p).ToList();
            selItems.Reverse();
            foreach (PoetItem item in selItems)
            {
                int i = pi.IndexOf(item);
                if (i < poetsListView.Items.Count - 1)
                {
                    pi.RemoveAt(i);
                    pi.Insert(i + 1, item);
                }
            }
            foreach (PoetItem item in selItems)
            {
                poetsListView.SelectedItems.Add(item);
            }
            poetsListView.ScrollIntoView(selItems[0]);
        }

        private void ResetOrderButton_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<int, int> orders;
            ObservableCollection<PoetItem> p1 = App.dataSource.GetListOfPoets();

            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            localSettings.Values["poetOrders"] = defaultPoetOrder;
            orders = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<int, int>>(localSettings.Values["poetOrders"].ToString());

            foreach (var item in p1)
            {
                item.OrderID = orders[item.PoetID];
                item.ImageVisibility = Windows.UI.Xaml.Visibility.Collapsed;
            }

            p1 = new ObservableCollection<PoetItem>(p1.OrderBy(a => a.OrderID));

            this.defaultViewModel["Poets"] = p1;

            ObservableCollection<PoetItem> pp = new ObservableCollection<PoetItem>();
            pp.Add(new PoetItem() { PoetID = -10, PoetName = "" });
            pp.Add(new PoetItem() { PoetID = -1, PoetName = "همه‌ی آثار" });
            foreach (var item in p1)
            {
                pp.Add(item);
            }

            this.defaultViewModel["Sources"] = pp;

            try { searchSource.SelectedIndex = 0; }
            catch { }

        }

        private void FinishEditPoetsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                poetsListView.SelectionMode = ListViewSelectionMode.None;
                ObservableCollection<PoetItem> pi = (ObservableCollection<PoetItem>)this.defaultViewModel["Poets"];
                foreach (PoetItem item in pi)
                {
                    item.ImageVisibility = Windows.UI.Xaml.Visibility.Visible;
                }
                pi.Move(0, 0);


                ObservableCollection<PoetItem> pp = new ObservableCollection<PoetItem>();
                pp.Add(new PoetItem() { PoetID = -10, PoetName = "" });
                pp.Add(new PoetItem() { PoetID = -1, PoetName = "همه‌ی آثار" });
                foreach (var item in pi)
                {
                    pp.Add(item);
                }

                this.defaultViewModel["Sources"] = pp;

                try { searchSource.SelectedIndex = 0; }
                catch { }

                Dictionary<int, int> orders = new Dictionary<int, int>();
                for (int i = 0; i < pi.Count; i++)
                {
                    pi[i].OrderID = i;
                    orders.Add(pi[i].PoetID, i);
                }
                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                localSettings.Values["poetOrders"] = Newtonsoft.Json.JsonConvert.SerializeObject(orders);
            }
            catch { }
            EditPoetsButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            MoveUpButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            MoveDownButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            ResetOrderButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            FinishEditPoetsButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;


        }

        private void FavSortByButton_Click(object sender, RoutedEventArgs e)
        {
            // Ensure we have an app bar
            if (BottomAppBar == null) return;

            // Get the button just clicked
            var replyButton = sender as AppBarButton;
            if (replyButton == null) return;

            // Get the attached flyout
            var replyFlyout = (Flyout)Resources["ReplyFlyout"];
            if (replyFlyout == null) return;

            // Close the app bar before opening the flyout
            replyFlyout.Opening += delegate (object o, object o1)
            {
                if (BottomAppBar != null && BottomAppBar.Visibility == Visibility.Visible)
                {
                    BottomAppBar.Visibility = Visibility.Collapsed;
                }
            };

            // Show the app bar after the flyout closes
            replyFlyout.Closed += delegate (object o, object o1)
            {
                if (BottomAppBar != null && BottomAppBar.Visibility == Visibility.Collapsed)
                {
                    BottomAppBar.Visibility = Visibility.Visible;
                }
            };

            var grid = replyFlyout.Content as Grid;
            if (grid == null) return;
            grid.Tapped += delegate (object o, TappedRoutedEventArgs args)
            {
                var transparentGrid = args.OriginalSource as Grid;
                if (transparentGrid != null)
                {
                    replyFlyout.Hide();
                }
            };

            // Use the ShowAt() method on the flyout to specify where exactly the flyout should be located

            replyFlyout.ShowAt(BottomAppBar);
        }

        private void FavSort1_Click(object sender, RoutedEventArgs e)
        {
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["favOrderBy"] = 2;
            ShowFavorites();

            // Get the attached flyout
            var replyFlyout = (Flyout)Resources["ReplyFlyout"];
            if (replyFlyout == null) return;

            replyFlyout.Hide();
        }

        private void FavSort2_Click(object sender, RoutedEventArgs e)
        {
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["favOrderBy"] = 1;
            ShowFavorites();

            // Get the attached flyout
            var replyFlyout = (Flyout)Resources["ReplyFlyout"];
            if (replyFlyout == null) return;

            replyFlyout.Hide();
        }

        private void SearchExact_Loaded(object sender, RoutedEventArgs e)
        {
            searchExact = (CheckBox)sender;
        }
    }
}