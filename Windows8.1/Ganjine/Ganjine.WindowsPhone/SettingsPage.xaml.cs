﻿using Ganjine.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Ganjine
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SettingsPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        public SettingsPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            App.CurPageArg = "";

            LimitSearch.IsOn = App.LimitSearch;

            currentTheme.Items.Add("سفید");
            currentTheme.Items.Add("سیاه");
            currentTheme.Items.Add("نارنجی+سفید");
            currentTheme.Items.Add("آبی+سیاه");
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            currentTheme.SelectedIndex = (int)localSettings.Values["currentTheme"];
            TileAnimation.IsOn = (bool)localSettings.Values["wideTileUpdate"];
            if (App.DefaultFont == null)
            {
                Font1.IsChecked = true;
            }
            else if (App.DefaultFont.Source.Contains("Nazanin"))
            {
                Font2.IsChecked = true;
            }
            else if (App.DefaultFont.Source.Contains("Kamran"))
            {
                Font3.IsChecked = true;
            }
            else if (App.DefaultFont.Source.Contains("IranNastaliq"))
            {
                Font4.IsChecked = true;
            }
            else //Some misconfiguration
            {
                Font1.IsChecked = true;
            }

            GoogleAnalytics.EasyTracker.GetTracker().SendView("SettingsPage");
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async void OKButton_Click(object sender, RoutedEventArgs e)
        {
            App.LimitSearch = LimitSearch.IsOn;

            if (Font1.IsChecked == true)
            {
                App.DefaultFont = null;
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "FontFlyoutName", Font1.Content.ToString(), 0);
            }
            else if (Font2.IsChecked == true)
            {
                App.DefaultFont = (FontFamily)App.Current.Resources["NazaninFontFamily"];
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "FontFlyoutName", Font2.Content.ToString(), 0);
            }
            else if (Font3.IsChecked == true)
            {
                App.DefaultFont = (FontFamily)App.Current.Resources["KamranFontFamily"];
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "FontFlyoutName", Font3.Content.ToString(), 0);
            }
            else if (Font4.IsChecked == true)
            {
                App.DefaultFont = (FontFamily)App.Current.Resources["NastaliqFontFamily"];
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("ItemPage", "FontFlyoutName", Font4.Content.ToString(), 0);
            }
            
            await App.dataSource.SaveSettings();


            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            localSettings.Values["wideTileUpdate"] = TileAnimation.IsOn;
            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Settings", "TileAnimation", TileAnimation.IsOn ? "1" : "0", 0);
            if (currentTheme.SelectedIndex != (int)localSettings.Values["currentTheme"])
            {
                MessageDialog md = new MessageDialog("برای تغییر رنگ پوسته‌ی برنامه لازم است برنامه بسته شده و دوباره باز شود. هم‌اکنون برنامه بسته خواهد شد. لطفاً آن‌را دوباره باز کنید.");
                await md.ShowAsync();
                localSettings.Values["currentTheme"] = currentTheme.SelectedIndex;
                await System.Threading.Tasks.Task.Delay(TimeSpan.FromMilliseconds(200));
                App.Current.Exit();
            }

            Frame.GoBack();

        }
    }
}
