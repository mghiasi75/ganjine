﻿using Ganjine.Common;
using Ganjine.DataModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Popups;
using Windows.UI.StartScreen;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Universal Hub Application project template is documented at http://go.microsoft.com/fwlink/?LinkID=391955

namespace Ganjine
{
    /// <summary>
    /// A page that displays an overview of a single group, including a preview of the items
    /// within the group.
    /// </summary
    public sealed partial class SectionPage : Page
    {
        private readonly NavigationHelper navigationHelper;
        private readonly ObservableDictionary defaultViewModel = new ObservableDictionary();
        private TextBox jumpTextBox;
        DataBridge1 mydb;

        public SectionPage()
        {
            this.InitializeComponent();


            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            App.ItemPageInitializeData = null;
            App.CurPageArg = e.NavigationParameter.ToString();
            DataBridge1 db = Newtonsoft.Json.JsonConvert.DeserializeObject<DataBridge1>(e.NavigationParameter.ToString());

            defaultViewModel["Title"] = db.title;
            List<PoemItem> l = new List<PoemItem>();
            int PoemItemsCount = 0;
            int PoemItemFoldersCount = 0;
            //Avoid hanging UI while we're loading from database.
            await Task.Run(() =>
                {
                    l.AddRange(App.dataSource.GetListOfChildren(db.id));
                    PoemItemFoldersCount = l.Count;
                    List<PoemItem> b = App.dataSource.GetListOfPoems(db.id);
                    l.AddRange(b);
                    PoemItemsCount = b.Count;
                });

            mydb = db;

            defaultViewModel["Items"] = l;
            itemListView.UpdateLayout();
            int vitemid = -1;
            if (Windows.Storage.ApplicationData.Current.LocalSettings.Values["ViewedItemID"] != null)
                int.TryParse(Windows.Storage.ApplicationData.Current.LocalSettings.Values["ViewedItemID"].ToString(), out vitemid);
            int selectedItemIndex = vitemid + ((vitemid == -1) ? 0 : PoemItemFoldersCount);
            if ((selectedItemIndex == -1) && (App.ViewedSectionID.ContainsKey(App.CurPageArg)))
            {
                selectedItemIndex = App.ViewedSectionID[App.CurPageArg];
                App.ViewedSectionID.Remove(App.CurPageArg);
            }
            if (selectedItemIndex > 0)
            {
                selectedItemIndex += 2;
                if (selectedItemIndex > (l.Count - 1))
                {
                    selectedItemIndex = l.Count - 1;
                }
                itemListView.ScrollIntoView(l[selectedItemIndex]);
                Windows.Storage.ApplicationData.Current.LocalSettings.Values["ViewedItemID"] = -1;
                //App.ViewedItemID = -1;
            }
            if (PoemItemsCount > 10)
            {
                RandomButton.Visibility = Visibility.Visible;
                JumpToButton.Visibility = Visibility.Visible;
            }
            PinButton.Visibility = SecondaryTile.Exists(mydb.id.ToString()) ? Visibility.Collapsed : Visibility.Visible;
            UnPinButton.Visibility = SecondaryTile.Exists(mydb.id.ToString()) ? Visibility.Visible : Visibility.Collapsed;

            GoogleAnalytics.EasyTracker.GetTracker().SendView("SectionPage");
            // TODO: Create an appropriate data model for your problem domain to replace the sample data.
            /* var group = await SampleDataSource.GetGroupAsync((string)e.NavigationParameter);
             this.DefaultViewModel["Group"] = group;*/
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            // TODO: Save the unique state of the page here.
        }

        /// <summary>
        /// Shows the details of an item clicked on in the <see cref="ItemPage"/>
        /// </summary>
        /// <param name="sender">The GridView displaying the item clicked.</param>
        /// <param name="e">Event data that describes the item clicked.</param>
        private void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            ItemClick(e.ClickedItem);
        }

        private async void ItemClick(object c)
        {
            PoemItem poem = (PoemItem)(c);

            if (poem.Type == PoemItemType.Category)
            {
                DataBridge1 db = new DataBridge1()
                {
                    id = poem.ID,
                    title = poem.Text,
                    poetid = mydb.poetid,
                    rootPath = defaultViewModel["Title"].ToString() + " - " + mydb.rootPath
                };

                int idx = 0;
                for (int i = 0; i < itemListView.Items.Count; i++)
                {
                    if (((PoemItem)itemListView.Items[i]).ID == poem.ID)
                    {
                        idx = i;
                        break;
                    }
                }

                if (App.ViewedSectionID.ContainsKey(App.CurPageArg))
                {
                    App.ViewedSectionID.Remove(App.CurPageArg);
                }
                App.ViewedSectionID.Add(App.CurPageArg, idx);

                Frame.Navigate(typeof(SectionPage), Newtonsoft.Json.JsonConvert.SerializeObject(db));
            }
            else
            {
                DataBridge2 db = new DataBridge2()
                {
                    parentid = mydb.id,
                    id = poem.ID,
                };
                /*LoadingProgressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
                {
                    Storyboard sb = this.Resources["ListHide"] as Storyboard;
                    sb.Begin();
                    AppBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    this.UpdateLayout();
                });*/
                //await Task.Delay(60);
                /*await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {*/
                Frame.Navigate(typeof(ItemPage), Newtonsoft.Json.JsonConvert.SerializeObject(db));
                /*});*/
            }


            /* var itemId = ((SampleDataItem)e.ClickedItem).UniqueId;
             if (!Frame.Navigate(typeof(ItemPage), itemId))
             {
                 var resourceLoader = ResourceLoader.GetForCurrentView("Resources");
                 throw new Exception(resourceLoader.GetString("NavigationFailedExceptionMessage"));
             }*/
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.None;
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion


        private void RandomAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            Random r = new Random();
            ItemClick(itemListView.Items[r.Next(0, itemListView.Items.Count - 1)]);
        }

        private void TextBox_Loaded(object sender, RoutedEventArgs e)
        {
            jumpTextBox = (TextBox)sender;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ItemClick(itemListView.Items[int.Parse(jumpTextBox.Text) - 1]);
                ((Button)sender).IsEnabled = false;
            }
            catch
            {
                var dialog = new MessageDialog("عدد وارد شده صحیح نیست.");
                dialog.ShowAsync();
            }
        }

        private void TextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            ((TextBlock)sender).Text = "(از ۱ تا " + itemListView.Items.Count + ")";
        }

        private void JumpToAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            if (jumpTextBox != null)
            {
                jumpTextBox.Text = "";
            }
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(AboutPage));
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SettingsPage));
        }

        private async void PinAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var secondaryTile = new SecondaryTile(
                                    mydb.id.ToString(),
                                    defaultViewModel["Title"].ToString() + " - " + mydb.rootPath,
                                    Newtonsoft.Json.JsonConvert.SerializeObject(mydb),
                                    new Uri("ms-appx:///Assets/LargePics/" + mydb.poetid + ".jpg", UriKind.Absolute),
                                    TileSize.Square150x150);
                secondaryTile.VisualElements.ShowNameOnSquare150x150Logo = true;
                secondaryTile.VisualElements.ShowNameOnWide310x150Logo = true;
                secondaryTile.VisualElements.Wide310x150Logo = secondaryTile.VisualElements.Square150x150Logo;
                //bool isPinned = await secondaryTile.RequestCreateForSelectionAsync(;
                //secondaryTile.RequestDeleteAsync();
                bool isPinned = await secondaryTile.RequestCreateAsync();
            }
            catch { }
            finally
            {
                PinButton.Visibility = Visibility.Collapsed;
                UnPinButton.Visibility = Visibility.Visible;

                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("SectionPage", "Pin", defaultViewModel["Title"].ToString() + " - " + mydb.rootPath, 0);
            }
        }

        private async void UnPinAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var secondaryTile = new SecondaryTile(mydb.id.ToString());

                await secondaryTile.RequestDeleteAsync();
            }
            catch { }
            finally
            {
                PinButton.Visibility = Visibility.Visible;
                UnPinButton.Visibility = Visibility.Collapsed;

                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("SectionPage", "Unpin", defaultViewModel["Title"].ToString() + " - " + mydb.rootPath, 0);
            }
        }
    }
}