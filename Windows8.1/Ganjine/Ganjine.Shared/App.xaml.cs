﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Ganjine.Common;
using SQLitePCL;
using System.Threading.Tasks;
using Windows.Storage;
using Ganjine.DataModel;
using Windows.UI.Popups;

#if WINDOWS_PHONE_APP
using Windows.ApplicationModel.Email;
#endif

#if WINDOWS_APP
using Windows.UI.ApplicationSettings;
using Windows.UI.Popups;
#endif

// The Universal Hub Application project template is documented at http://go.microsoft.com/fwlink/?LinkID=391955

namespace Ganjine
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App : Application
    {

        public static SQLiteConnection conn;
        public static DataSource dataSource;
        public static List<PoemItem> favorites = new List<PoemItem>();
        public static string SearchQuery = "";
        public static FontFamily DefaultFont;
        public static bool LimitSearch;
        public string sErrorLog = "";
        public static string CurPageArg = "";
        public static bool askForReview = false;
        public static int SearchSourceIndex = 0;
        
#if WINDOWS_PHONE_APP
        private TransitionCollection transitions;
        public static Dictionary<string, int> ViewedSectionID = new Dictionary<string, int>();
        public static DataBridge2 ItemPageInitializeData = null;
#endif


        /// <summary>
        /// Initializes the singleton instance of the <see cref="App"/> class. This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {

            this.InitializeComponent();

            this.Suspending += this.OnSuspending;


#if WINDOWS_APP
            RequestedTheme = ApplicationTheme.Light;
#endif
#if WINDOWS_PHONE_APP
            //RequestedTheme = ApplicationTheme.Dark;

            //this.UnhandledException += Application_UnhandledException;

            
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            Object theme = localSettings.Values["currentTheme"];


            if (theme == null)
            {
                // No data
                localSettings.Values["currentTheme"] = 0;
                RequestedTheme = ApplicationTheme.Light;
            }
            else
            {
                if (((int)theme == 1) || ((int)theme == 3))
                {
                    RequestedTheme = ApplicationTheme.Dark;
                }
                else
                {
                    RequestedTheme = ApplicationTheme.Light;
                }
            }

            Object value = localSettings.Values["runCount"];


            if (value == null)
            {
                // No data
                localSettings.Values["runCount"] = 1;
            }
            else
            {
                // Access data in value
                if ((int.Parse(localSettings.Values["runCount"].ToString()) > 6 ) && (localSettings.Values["AskedForReview"] == null))
                {
                    askForReview = true;
                }
                localSettings.Values["runCount"] = int.Parse(localSettings.Values["runCount"].ToString()) + 1;
            }


#endif
        }

#if WINDOWS_PHONE_APP
        //REMOVED!
        private async void Application_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            sErrorLog = e.Message + "\r\n" + e.Exception.ToString();


            var messageDialog = new MessageDialog("اپلیکیشن دچار خطا شد. آیا مایل به ارسال جزئیات خطا برای ما هستید؟ این کار به رفع باگ‌های احتمالی کمک می‌کند.");

            // Add commands and set their callbacks; both buttons use the same callback function instead of inline event handlers
            messageDialog.Commands.Add(new UICommand(
                "بله",
                new UICommandInvokedHandler(this.SendErrorHandler)));
            messageDialog.Commands.Add(new UICommand(
                "خیر",
                new UICommandInvokedHandler(this.DontSendErrorHandler)));

            // Set the command that will be invoked by default
            messageDialog.DefaultCommandIndex = 0;

            // Set the command to be invoked when escape is pressed
            messageDialog.CancelCommandIndex = 1;

            // Show the message dialog
            await messageDialog.ShowAsync();
        }


        private void DontSendErrorHandler(IUICommand command)
        {
            
        }

        private async void SendErrorHandler(IUICommand command)
        {
            EmailMessage mail = new EmailMessage();
            mail.Subject = "BugReport: Ganjine for Windows Phone v2.8";
            mail.Body = sErrorLog;
            mail.To.Add(new Windows.ApplicationModel.Email.EmailRecipient("mghiasi75@gmail.com", "Mahdi Ghiasi"));
            await Windows.ApplicationModel.Email.EmailManager.ShowComposeNewEmailAsync(mail);
            sErrorLog = "";
        }
#endif

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected async override void OnLaunched(LaunchActivatedEventArgs e)
        {
            dataSource = new DataSource();

            dataSource.InitializeSettings();
            dataSource.InitializeFavorites();

#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif

            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                //Associate the frame with a SuspensionManager key                                
                SuspensionManager.RegisterFrame(rootFrame, "AppFrame");

                // TODO: change this value to a cache size that is appropriate for your application
                rootFrame.CacheSize = 1;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    // Restore the saved session state only when appropriate
                    try
                    {
                        
                        await SuspensionManager.RestoreAsync();
                        SearchQuery = SuspensionManager.SessionState["SearchQuery"].ToString();
                        SearchSourceIndex = int.Parse(SuspensionManager.SessionState["SearchSourceIndex"].ToString());
                    }
                    catch (SuspensionManagerException)
                    {
                        // Something went wrong restoring state.
                        // Assume there is no state and continue
                    }
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
#if WINDOWS_PHONE_APP
                // Removes the turnstile navigation for startup.
                if (rootFrame.ContentTransitions != null)
                {
                    this.transitions = new TransitionCollection();
                    foreach (var c in rootFrame.ContentTransitions)
                    {
                        this.transitions.Add(c);
                    }
                }

                rootFrame.ContentTransitions = null;
                rootFrame.Navigated += this.RootFrame_FirstNavigated;
                rootFrame.Navigated += rootFrame_Navigated;
#endif



                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if (!rootFrame.Navigate(typeof(HubPage), e.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }

            
            // If the app was activated from a secondary tile, open to the correct restaurant
            if (!String.IsNullOrEmpty(e.Arguments))
            {
                if ((rootFrame.BackStack.Count == 0) || (CurPageArg != e.Arguments))
                {
                    while (rootFrame.BackStackDepth > 0)
                    {
                        rootFrame.GoBack();
                    }
                    rootFrame.Navigate(typeof(SectionPage), e.Arguments);
                }
            }

            // Ensure the current window is active
            Window.Current.Activate();
        }

       

#if WINDOWS_PHONE_APP
        /// <summary>
        /// Restores the content transitions after the app has launched.
        /// </summary>
        /// <param name="sender">The object where the handler is attached.</param>
        /// <param name="e">Details about the navigation event.</param>
        private void RootFrame_FirstNavigated(object sender, NavigationEventArgs e)
        {
            var rootFrame = sender as Frame;
            rootFrame.ContentTransitions = this.transitions ?? new TransitionCollection() { new NavigationThemeTransition() };
            rootFrame.Navigated -= this.RootFrame_FirstNavigated;
        }

         void rootFrame_Navigated(object sender, NavigationEventArgs e)
        {
            var rootFrame = sender as Frame;
            if ((e.NavigationMode == NavigationMode.Back) && (rootFrame.ForwardStack.Count > 0) && (rootFrame.ForwardStack[rootFrame.ForwardStack.Count - 1].SourcePageType == typeof(ItemPage)))
            {
                ItemPageInitializeData = null;
            }
        }
#endif

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();

            SuspensionManager.SessionState["SearchQuery"] = SearchQuery;
            SuspensionManager.SessionState["SearchSourceIndex"] = SearchSourceIndex;
            await SuspensionManager.SaveAsync();
            deferral.Complete();
        }


#if WINDOWS_APP

        protected override void OnWindowCreated(WindowCreatedEventArgs args)
        {
            SettingsPane.GetForCurrentView().CommandsRequested += OnCommandsRequested;
        }

        private void OnCommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            var setting = new SettingsCommand("تنظیمات", "تنظیمات", handler =>
                new MySettingsFlyout().Show());
            args.Request.ApplicationCommands.Add(setting);
            var about = new SettingsCommand("درباره", "درباره", handler =>
                new MyAboutFlyout().Show());
            args.Request.ApplicationCommands.Add(about);
            var pp = new SettingsCommand("Privacy Policy", "Privacy Policy", handler =>
                new MyPrivacyFlyout().Show());
            args.Request.ApplicationCommands.Add(pp);
        }

#endif
    }
}