﻿//
// GanjineBackgroundTask.cs
//

using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace GanjineBackgroundTaskProj
{
    public sealed class GanjineBackgroundTask : IBackgroundTask
    {

        static int[] poetsWithPhoto = new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20, 22, 23, 24, 27, 28, 35, 54, 501, 502, 503, 504, 506, 612, 701 };

        public void Run(IBackgroundTaskInstance taskInstance)
        {
            //BackgroundTaskDeferral _deferral = taskInstance.GetDeferral();
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            if ((bool)localSettings.Values["wideTileUpdate"] == true)
            {
                UpdateMainTile();
            }

            //_deferral.Complete();
        }

        public static void UpdateMainTile()
        {
            try
            {
                TileUpdateManager.CreateTileUpdaterForApplication().Clear();
                var tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150ImageCollection);
                var tileImage = tileXml.GetElementsByTagName("image");
                var tileImageTemplate = (tileImage[0] as XmlElement).CloneNode(true) as XmlElement;
                var parentNode = (tileImage[0] as XmlElement).ParentNode;
                for (int i = tileImage.Count - 1; i >= 0; i--)
                {
                    parentNode.RemoveChild(tileImage[i]);
                }
                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                object value = localSettings.Values["runCount"];
                int[] poetIDs = GetRandomPoetWithPhotoIDs(5);

                for (int i = 0; i < 5; i++)
                {
                    var newImage = tileImageTemplate.CloneNode(true) as XmlElement;
                    newImage.SetAttribute("id", (i + 1).ToString());
                    newImage.SetAttribute("src", "ms-appx:///Assets/LargePics/" + (poetIDs[i]).ToString() + ".jpg");
                    parentNode.AppendChild(newImage);
                }

                var tileNotification = new TileNotification(tileXml);
                TileUpdateManager.CreateTileUpdaterForApplication().Update(tileNotification);
            }
            catch { }
        }

        /// <summary>
        /// Picks random selection of available poet photos.
        /// </summary>
        public static int[] GetRandomPoetWithPhotoIDs(int count)
        {
            var totalGameIDs = poetsWithPhoto.Length;
            if (count > totalGameIDs) count = totalGameIDs;

            var rnd = new Random();
            var leftToPick = count;
            var itemsLeft = totalGameIDs;
            var arrPickIndex = 0;
            var returnIDs = new List<int>();
            while (leftToPick > 0)
            {
                if (rnd.Next(0, itemsLeft) < leftToPick)
                {
                    returnIDs.Add(poetsWithPhoto[arrPickIndex]);
                    leftToPick--;
                }
                arrPickIndex++;
                itemsLeft--;
            }

            return returnIDs.ToArray();
        }

    }
}
